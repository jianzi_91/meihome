<?php

namespace App\Models;

use App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class StoreBusinessHour extends Model
{
    protected $guarded = ['created_at', 'updated_at'];

    public static function checkBusinessHour($store_id, $timezone, $now, $day_of_week, $previous_day = null)
    {
        $bizHours = StoreBusinessHour::where('store_id', $store_id)->where('day', $day_of_week)->first();
        if ($bizHours) {
            if ($bizHours->status) {
                if ($bizHours->open_time && $bizHours->close_time) {
                    if ($previous_day) {
                        $open = Carbon::createFromFormat('Y-m-d H:i:s', $previous_day->toDateString() . ' ' . $bizHours->open_time, $timezone);
                        $close = Carbon::createFromFormat('Y-m-d H:i:s', $previous_day->toDateString() . ' ' .  $bizHours->close_time, $timezone);
                    }
                    else {
                        $open = Carbon::createFromFormat('H:i:s', $bizHours->open_time, $timezone);
                        $close = Carbon::createFromFormat('H:i:s', $bizHours->close_time, $timezone);
                    }

                    if ($open->diffInMinutes($close, false) < 0) {
                        $close->addDay();
                    }

                    if (!$now->between($open, $close)) {
                        return false;
                    }

                    return true;
                }
            }
        }

        return false;
    }
}