<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;

class ParentOrder extends Model
{
    use GlobalEloquentBuilder;

    protected $table = 'parent_orders';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function customer()
    {
        return $this->hasOne(Customer::class, 'cus_id', 'customer_id');
    }

    public function items()
    {
        return $this->hasMany(Order::class, 'parent_order_id', 'id');
    }

    public function shipping_address()
    {
        return $this->hasOne(Shipping::class, 'parent_order_id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(TaxInvoice::class, 'parent_order_id');
    }

    public function getIsDeliveryOrderExistAttribute()
    {
        $flagExist = false;

        foreach($this->invoices as $taxInvoice) {
           if ($taxInvoice->deliveries->count()) {
                $flagExist = true;
           }
        }

        return $flagExist;
    }

    public function deliveries()
    {
        return $this->hasManyThrough(DeliveryOrder::class, TaxInvoice::class, 'parent_order_id', 'tax_invoice_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany(OrderOnlineLog::class, 'parent_order_id', 'id');
    }

    public function getDeliveryOrderVersionAttribute()
    {
        return \Carbon\Carbon::createFromTimestamp(strtotime($this->created_at))->timezone('Asia/Kuala_Lumpur') > getInvoiceDate() ? 2 : 1;
    }

    public function getPaymentTypeAttribute()
    {
        return $this->items->first()->payment_method;
    }
}
