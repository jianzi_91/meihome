<?php

namespace App\Models;

use App\Repositories\AttributeRepo;
use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;

class Order extends Model
{
    use GlobalEloquentBuilder;

    protected $table = 'nm_order';
    protected $primaryKey = 'order_id';

    public function generatedCode()
    {
        return $this->hasMany(GeneratedCode::class, 'order_id');
    }

    public function coupons()
    {
        return $this->generatedCode()->where('type', 2);
    }

    public function tickets()
    {
        return $this->generatedCode()->where('type', 3);
    }

    public function ecards()
    {
        return $this->generatedCode()->where('type', 4);
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'cus_id', 'order_cus_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'order_pro_id', 'pro_id');
    }

    public function courier()
    {
        return $this->hasOne(Courier::class, 'id', 'order_courier_id');
    }

    public function total()
    {
        return collect([
            'price' => round(max($this->{'currency_rate'} > 0 ? round($this->{'order_vtokens'} * $this->{'currency_rate'}) : $this->{'total_product_price'} + $this->{'total_product_shipping_fees'}, 0), 2),
            'credit' => round(max($this->{'order_vtokens'}, 0), 4),
            'platform_charge_rate' => round(max($this->{'cus_platform_charge_rate'}, 0), 4),
            'platform_charge_credit' => round(max($this->{'cus_platform_charge_value'}, 0), 4),
            'service_charge_rate' => round(max($this->{'cus_service_charge_rate'}, 0), 4),
            'service_charge_credit' => round(max($this->{'cus_service_charge_value'}, 0), 4),
            'merchant_charge_rate' => round(max($this->{'merchant_charge_percentage'}, 0), 4),
            'merchant_charge_credit' => round(max($this->{'merchant_charge_vtoken'}, 0), 4),
            'shipping_fees_credit' => round(max($this->{'total_product_shipping_fees_credit'} ,0), 4),
            'merchant_earn_credit' => round(max($this->{'order_vtokens'} - ($this->{'merchant_charge_vtoken'} + $this->{'cus_service_charge_value'} + $this->{'cus_platform_charge_value'}) ,0), 4),
        ]);
    }

    public function unrounded_total()
    {
        return collect([
            'price' => max($this->{'currency_rate'} > 0 ? $this->{'order_vtokens'} * $this->{'currency_rate'} : $this->{'total_product_price'} + $this->{'total_product_shipping_fees'}, 0),
            'credit' => round(max($this->{'order_vtokens'}, 0), 4),
            'platform_charge_rate' => round(max($this->{'cus_platform_charge_rate'}, 0), 4),
            'platform_charge_credit' => round(max($this->{'cus_platform_charge_value'}, 0), 4),
            'service_charge_rate' => round(max($this->{'cus_service_charge_rate'}, 0), 4),
            'service_charge_credit' => round(max($this->{'cus_service_charge_value'}, 0), 4),
            'merchant_charge_rate' => round(max($this->{'merchant_charge_percentage'}, 0), 4),
            'merchant_charge_credit' => round(max($this->{'merchant_charge_vtoken'}, 0), 4),
            'shipping_fees_credit' => round(max($this->{'total_product_shipping_fees_credit'} ,0), 4),
            'merchant_earn_credit' => round(max($this->{'order_vtokens'} - ($this->{'merchant_charge_vtoken'} + $this->{'cus_service_charge_value'} + $this->{'cus_platform_charge_value'}) ,0), 4),
        ]);
    }

    public function status()
    {
        if($this->{'order_type'} == 1)
        {
            switch ($this->{'order_status'}) {
                case 1:
                    return trans('localize.processing');
                    break;

                case 2:
                    return trans('localize.packaging');
                    break;

                case 3:
                    if($this->{'product_shipping_fees_type'} <3)
                    {
                        return trans('localize.shipped');
                    }
                    else
                    {
                        return trans('localize.arranged');
                    }
                    break;

                case 4:
                    return trans('localize.completed');
                    break;

                case 5:
                    return trans('localize.cancelled');
                    break;

                case 6:
                    return trans('localize.refunded');
                    break;
            }
        }

        if(in_array($this->{'order_type'}, [3,4,5]))
        {
            switch ($this->{'order_status'}) {

                case 2:
                    return trans('localize.pending');
                    break;

                case 4:
                    return trans('localize.completed');
                    break;

                case 5:
                    return trans('localize.cancelled');
                    break;

                case 6:
                    return trans('localize.refunded');
                    break;
            }
        }

        return null;
    }

    public function shipping_type()
    {
        switch ($this->{'product_shipping_fees_type'}) {
            case 0:
                return trans('localize.shipping_fees_free');
                break;

            case 1:
                return trans('localize.shipping_fees_product');
                break;

            case 2:
                return trans('localize.shipping_fees_transaction');
                break;

            case 3:
                return trans('localize.self_pickup');
                break;

            default:
                return null;
                break;
        }
    }

    public function mapping()
    {
        return $this->hasOne(OrderMapping::class, 'order_id', 'order_id');
    }

    public function getParseOrderAttributeAttribute()
    {
        if (is_null($this->order_attributes)) {
            return '';
        }

        $str = '';
        $attributes = json_decode($this->order_attributes);

        foreach ($attributes as $parent => $child)
        {
            $str .= "<b>$parent : </b> $child </br>";
        }

        return $str;
    }

    public function order_mappings()
    {
        return $this->hasOne(OrderMapping::class, 'order_id', 'order_id');
    }

    public function type()
    {
        switch ($this->{'order_type'}) {
            case 1:
                return trans('localize.normal_product');
                break;

            case 2:
                return 'Deals';
                break;

            case 3:
                return trans('localize.coupon');
                break;

            case 4:
                return trans('localize.ticket');
                break;

            case 5:
                return trans('localize.e-card.name');
                break;

            default:
                return null;
                break;
        }
    }

    public function pricing()
    {
        return $this->hasOne(ProductPricing::class, 'id', 'order_pricing_id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'co_curcode', 'currency');
    }

    public function logs()
    {
        return $this->hasMany(OrderOnlineLog::class, 'order_id', 'order_id');
    }
}
