<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderOnlineLog extends Model
{
    protected $table = 'order_online_log';
    protected $guarded = ['created_at', 'updated_at'];

    public function user_type()
    {
        switch ($this->user_type)
        {
            case 0:
                return trans('localize.system');
                break;

            case 1:
                return trans('localize.customer');
                break;

            case 2:
                return trans('localize.Merchant');
                break;

            case 3:
                return trans('localize.admin');
                break;

            default:
                return "";
                break;
        }
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'cus_id', 'user_id');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'mer_id', 'user_id');
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'adm_id', 'user_id');
    }

    public function updaterInfo()
    {
        switch ($this->user_type)
        {
            case 0:
                return null;
                break;

            case 1:
                return $this->customer();
                break;

            case 2:
                return $this->merchant();
                break;

            case 3:
                return $this->admin();
                break;

            default:
                return null;
                break;
        }
    }


}
