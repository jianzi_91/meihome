<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ProductImage extends Authenticatable
{

    protected $table = 'nm_product_image';
    protected $fillable = ['pro_id', 'title','image','status','order','main'];
    protected $primaryKey = 'id';

    public function product()
    {
        return $this->hasOne(Product::class, 'pro_id', 'pro_id');
    }

    public function getImagePathAttribute()
    {
        $product = $this->product()->getResults();
        if(!$this->{'image'} || !$product || !$product->pro_mr_id)
        {
            return url('web/images/stock.png');
        }

        return env('IMAGE_DIR') . '/product/' . $product->pro_mr_id . '/' . $this->{'image'};
    }
}