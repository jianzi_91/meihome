<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SenangPay extends Model
{
	protected $table = 'senangpay_logs';
    protected $guarded = [];

    const STATUS = [
    	'INITIATED' => 0,
    	'ACTIVE' => 1,
    	'COMPLETED' => 2,
    	'FAILED' => 3,
    ];
}
