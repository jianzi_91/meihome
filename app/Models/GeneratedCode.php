<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Helper;

class GeneratedCode extends Authenticatable
{
    protected $table = 'generated_codes';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';

    public function isExpired()
    {
        $valid_to = $this->{'valid_to'};
        $status = $this->{'status'};

        if(empty($valid_to))
            return false;

        $valid_to = Carbon::createFromFormat('Y-m-d H:i:s', $valid_to, 'UTC');
        $now = Carbon::now('UTC');

        if($status == 1 && $now->gte($valid_to))
            return true;

        return false;
    }
}
