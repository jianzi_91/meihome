<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;

class OrderOffline extends Model
{
    use GlobalEloquentBuilder;

    protected $table = 'order_offline';
    protected $guarded = ['created_at', 'updated_at'];

    public function wallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function store()
    {
        return $this->hasOne(Store::class, 'stor_id', 'store_id');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'cus_id', 'cust_id');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'mer_id', 'mer_id');
    }

    public function status()
    {
        switch ($this->status) {
            case 0:
                return trans('localize.unpaid');
                break;

            case 1:
                return trans('localize.paid');
                break;

            case 2:
                return trans('localize.cancel_by_member');
                break;

            case 3:
                return trans('localize.cancel_by_merchant');
                break;

            case 4:
                return trans('localize.refunded');
                break;
        }

        return null;
    }

    public function charges()
    {
        $currencyRate = $this->currency_rate;

        return json_decode(json_encode([
            'credit' => [
                'amount' => $this->v_token,
                'platformCharge' => $this->merchant_platform_charge_token,
                'customerCharge' => $this->customer_charge_token,
                'merchantCharge' => $this->merchant_charge_token,
                'merchantEarn' => round($this->v_token - $this->merchant_charge_token, 4),
                'total' => $this->order_total_token,
            ],
            'price' => [
                'amount' => round($this->v_token * $currencyRate, 2),
                'platformCharge' => round($this->merchant_platform_charge_token * $currencyRate, 2),
                'customerCharge' => round($this->customer_charge_token * $currencyRate, 2),
                'merchantCharge' => round($this->merchant_charge_token * $currencyRate, 2),
                'merchantEarn' => round(($this->v_token - $this->merchant_charge_token) * $currencyRate, 2),
                'total' => round($this->order_total_token * $currencyRate, 4),
            ]
        ]));
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'co_curcode', 'currency');
    }

    public function tax_invoice_number($running = 'OFA')
    {
        if(!$this->tax_inv_no)
        {
            return '';
        }

        if($this->country)
        {
            return "{$this->country->co_code}-{$running}{$this->tax_inv_no}";
        }

        return "OFA{$this->tax_inv_no}";
    }
}
