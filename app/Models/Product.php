<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\GlobalEloquentBuilder;
use App;

class Product extends Model
{
    use GlobalEloquentBuilder;

    protected $table = 'nm_product';
    protected $guarded = ['pro_id'];
    protected $primaryKey = 'pro_id';

    public function getTitleAttribute($value)
    {
        $default_title = $this->{'pro_title_en'};
        $title = $this->{'pro_title_'.App::getLocale()};

        if (empty($title)) {
            return $default_title;
        } else {
            return $title;
        }
    }

    public function getDescAttribute($value)
    {
        $default_desc = $this->{'pro_desc_en'};
        $desc = $this->{'pro_desc_'.App::getLocale()};

        if (empty($desc)) {
            return $default_desc;
        } else {
            return $desc;
        }
    }

    public function getShortDescAttribute($value)
    {
        $defaultShortDesc = $this->{'short_desc_en'};
        $shortDesc = $this->{'short_desc_'.App::getLocale()};

        if (empty($shortDesc))
            return $defaultShortDesc;
        return $shortDesc;
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'pro_id', 'pro_id')->orderBy('main', 'desc')->orderBy('order', 'asc');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'mer_id', 'pro_mr_id');
    }

    public function store()
    {
        return $this->hasOne(Store::class, 'stor_id', 'pro_sh_id');
    }

    public function pricing()
    {
        return $this->hasMany(ProductPricing::class, 'pro_id', 'pro_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'nm_product_category', 'product_id', 'category_id');
    }

    public function pricings()
    {
        return $this->hasMany(ProductPricing::class, 'pro_id');
    }

    public function status()
    {
        switch ($this->pro_status)
        {
            case '0':
                return trans('localize.Inactive');
                break;

            case '1':
                return trans('localize.Active');
                break;

            case '2':
                return trans('localize.incomplete');
                break;

            case '3':
                return trans('localize.pending_review');
                break;

            default:
                return '';
                break;
        }
    }

    public function type()
    {
        switch ($this->pro_type)
        {
            case '1':
                return trans('localize.normal_product');
                break;

            case '2':
                return trans('localize.coupon');
                break;

            case '3':
                return trans('localize.ticket');
                break;

            case '4':
                return trans('localize.e-card.name');
                break;

            default:
                return '';
                break;
        }
    }
}
