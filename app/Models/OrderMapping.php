<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderMapping extends Model
{
    protected $table = 'order_mappings';
    protected $primaryKey = 'id';

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'order_id');
    }

    public function delivery_order()
    {
        return $this->hasOne(DeliveryOrder::class, 'id', 'delivery_order_id');
    }

    public function invoice()
    {
        return $this->hasOne(TaxInvoice::class, 'id', 'tax_invoice_id');
    }
}
