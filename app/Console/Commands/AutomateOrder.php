<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AutomateOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automateOrder {operation}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process order based on selected operation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $option = $this->argument('operation');

            switch ($option) {
                case 'online_orders_daily':
                    \App::call('App\Http\Controllers\Cron\ProcessController@online_orders_daily');
                    break;

                case 'serial_number_orders_daily':
                \App::call('App\Http\Controllers\Cron\ProcessController@serial_number_orders_daily');
                    break;

                default:
                $this->info('Invalid operation');
                    break;
            }

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
