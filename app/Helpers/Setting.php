<?php

use App\Models\Country;

function apiResponse($status, $message)
{
    $payload = [
        'status' => $status
    ];

    if (is_array($message)) {
        $payload = array_merge($payload, $message);
    } else {
        $payload['message'] = $message;
    }

    return response()->json($payload);
}

function getQuerySearchText($string, $startOnly = false, $endOnly = false)
{
    if($startOnly)
    {
        return "%$string";
    }

    if($endOnly)
    {
        return "$string%";
    }

    return "%$string%";
}

function stdObject($array)
{
    return json_decode(json_encode($array));
}

function buildHttpQueryUrl($request, $additional = null, $url = null)
{
    if(is_null($url))
    {
        $url = url()->current();
    }

    if(!is_array($request))
    {
        return $url;
    }

    if(is_array($additional) && !empty($additional))
    {
        $request = array_merge($request, $additional);
    }

    if(empty($request))
    {
        return $url;
    }

    $request = array_filter($request);

    $query = http_build_query($request);

    return "$url?$query";
}

function chargeNames()
{
    return stdObject([
        'platform' => trans('localize.platform_charges'),
        'service' => trans('localize.gst'),
        'merchant' => trans('localize.merchant_charge'),
        'shipping' => trans('localize.shipping_fees'),
    ]);
}

function getCountryLocale($getId = null)
{
    $country_id = null;

    if(\Cookie::get('country_locale'))
    {
        $country_id = \Cookie::get('country_locale');
    }
    elseif(session('countryid'))
    {
        $country_id = session('countryid');
    }

    $country = Country::find($country_id);

    if(!$country)
    {
        return null;
    }

    if($getId)
    {
        return $country->co_id;
    }

    return $country;
}

function setCountryLocale($country_id)
{
    $country = Country::find($country_id);

    if(!$country)
    {
        return false;
    }

    session(['countryid' => $country->co_id]);
    session(['timezone' => $country->timezone]);
    \Cookie::queue(\Cookie::forever('country_locale', $country->co_id));
    \Cookie::queue(\Cookie::forever('timezone', $country->timezone));
    \Cookie::queue(\Cookie::forever('countryid', $country->co_id));

    return true;
}

function sanitizeSerialNumber($string)
{
    $string = preg_split("/[-,_,:|]/", $string);
    if(count($string) == 2)
    {
        $string = $string[0] . "-" . filter_var($string[1], FILTER_SANITIZE_NUMBER_INT);
    }
    else
    {
        $string = current($string);
    }

    return $string;
}

function isRestricted($country_id, $byCountryLocale = true, $compared_id = null)
{
    $restricted = config('app.cart.restricted.countries') ? explode(',', config('app.cart.restricted.countries')) : [];

    //check if country is in restricted list
    if(!in_array($country_id, $restricted))
    {
        return false;
    }

    if(!$byCountryLocale)
    {
        if($compared_id && $country_id == $compared_id)
        {
            return false;
        }

        return true;
    }

    $country = getCountryLocale();

    if(!$country)
    {
        return false;
    }

    if($country_id <> $country->co_id)
    {
        return true;
    }

    return false;
}

function getInvoiceDate()
{
    return \Carbon\Carbon::parse(config('app.invoice.date'))->timezone(config('app.invoice.timezone'))->startOfDay();
}
