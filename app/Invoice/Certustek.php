<?php

namespace App\Invoice;

use Exception;
use SimpleXMLElement;
use SoapClient;

use App\Models\Order;
use App\Models\OrderOffline;
use App\Models\ParentOrder;
use App\Models\TaxInvoice;

class Certustek
{
    protected $soapWrapper;

    public function __construct(/*SoapWrapper $soapWrapper*/)
    {
        $this->url = config('certustek.url');
        $this->rentid = config('certustek.rentid');
        $this->source = config('certustek.source');

        // $this->soapWrapper = $soapWrapper;
    }

    public function createInvoiceV3($parentOrderId)
    {

        $local_invoices = TaxInvoice::where('parent_order_id',$parentOrderId)->get();
        $p_order = ParentOrder::find($parentOrderId);
        $transactions = Order::where('transaction_id', $p_order->transaction_id)->get();

        $customer = $p_order->customer;
        $response_count = 0;

        try {
            foreach($local_invoices as $local_invoice) {

                $merchant = $local_invoice->merchant;
                $taxes = $merchant->mer_platform_charge + $merchant->mer_service_charge;

                // Creating data for params for invoice
                $data = [
                    'OrderId' => $local_invoice->tax_number,
                    'OrderDate' => date('Y/m/d'),
                    'BuyerIdentifier' => $customer->identity_card,
                    'BuyerName' => $customer->cus_name,
                    'BuyerAddress' => $customer->cus_address1,
                    'BuyerPersonInCharge' => null,
                    'BuyerTelephoneNumber' => $customer->phone_area_code . $customer->cus_phone,
                    'BuyerFacsimileNumber' => null,
                    'BuyerEmailAddress' => $customer->email,
                    'BuyerCustomerNumber' => null,
                    'DonateMark' => 0,
                    'InvoiceType' => '07',
                    'CarrierType' => null,
                    'CarrierId1' => null,
                    'CarrierId2' => null,
                    'NPOBAN' => null,
                    'TaxType' => 1,
                    'TaxRate' => $taxes / 100,
                    'PayWay' => 5,
                    'Remark' => '',
                ];

                $xml_data = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?><Invoice XSDVersion='2.8'></Invoice>");

                $xml = $this->arrayToXml($data, $xml_data);

                $details = $xml->addChild('Details');

                $commission_sum = 0;
                $gst_sum = 0;

                foreach($local_invoice->items as $order) {

                    if ($order->product->pro_mr_id == $local_invoice->merchant_id) {
                        $productItem = $details->addChild('ProductItem');
                        $productItem->addChild('ProductionCode', $order->order_pro_id);
                        $productItem->addChild('Description', $order->product->pro_title_cnt ?: $order->product->pro_title_en);
                        $productItem->addChild('Quantity', $order->order_qty);
                        $productItem->addChild('Unit', null);
                        $productItem->addChild('UnitPrice', $order->product_price);
                        
                        $commission_sum += $order->total_product_price * $merchant->mer_platform_charge / 100;
                        $gst_sum += $order->total_product_price * $merchant->mer_service_charge / 100;
                    }
                }

                $gst_sum += $commission_sum * $merchant->mer_service_charge / 100;

                // 行政消费税
                $productItem = $details->addChild('ProductItem');
                $productItem->addChild('ProductionCode', $local_invoice->tax_number);
                $productItem->addChild('Description', '行政消费税');
                $productItem->addChild('Quantity', 1);
                $productItem->addChild('Unit', null);
                $productItem->addChild('UnitPrice', $commission_sum + $gst_sum);

                // $xml->asXML(resource_path('/xml/CreateInvoiceV3.xml'));

                $client = new SoapClient($this->url, [
                    'trace' => true,
                ]);

                $params = [
                    'invoicexml' => $xml->asXML(),
                    'hastax' => 1,
                    'rentid' => $this->rentid,
                    'source' => $this->source,
                ];

                // return $xml;
            
                $response = $client->CreateInvoiceV3($params);

                if (strlen($response->return) != 15) {
                    throw new Exception($response->return);
                }

                $local_invoice->tw_invoice = $response->return;
                $local_invoice->save();
            }

            return $local_invoices;
        }
        catch (Exception $e) {

            return $e->getMessage();
        }
        
    }

    // public function queryInvoice()
    // {
    //     $client = new SoapClient($this->url, [
    //         'trace' => true,
    //     ]);

    //     $params = [
    //         'invoicenumber' => 'JU11010000',
    //         'invoiceyear' => '2018',
    //         'rentid' => $this->rentid,
    //         'source' => $this->source,
    //     ];

    //     try {
    //         $response = $client->QueryInvoice($params);

    //         $xml = new SimpleXMLElement($response->return);

    //         return $xml;
    //     }
    //     catch (Exception $e) {
    //         // return back()->with('error', $e->getMessage());
    //         return $e->getMessage();
    //     }
    // }

    public function createOfflineInvoice($order_id, $order_tax_inv)
    {
        try {

            $order = OrderOffline::find($order_id);

            $customer = $order->customer;
            $merchant = $order->merchant;

            // Creating data for params for invoice
            $data = [
                'OrderId' => 'OFC' . $order_tax_inv,
                'OrderDate' => date('Y/m/d'),
                'BuyerIdentifier' => $customer->identity_card,
                'BuyerName' => $customer->cus_name,
                'BuyerAddress' => $customer->cus_address1,
                'BuyerPersonInCharge' => null,
                'BuyerTelephoneNumber' => $customer->phone_area_code . $customer->cus_phone,
                'BuyerFacsimileNumber' => null,
                'BuyerEmailAddress' => $customer->email,
                'BuyerCustomerNumber' => null,
                'DonateMark' => 0,
                'InvoiceType' => '07',
                'CarrierType' => null,
                'CarrierId1' => null,
                'CarrierId2' => null,
                'NPOBAN' => null,
                'TaxType' => 1,
                'TaxRate' => 0.05,
                'PayWay' => 5,
                'Remark' => '',
            ];

            $xml_data = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?><Invoice XSDVersion='2.8'></Invoice>");

            $xml = $this->arrayToXml($data, $xml_data);

            $details = $xml->addChild('Details');

            $commission = ceil($order->amount * $order->merchant_platform_charge_percentage / 100);
            $gst = ceil($order->amount * 0.05 + $commission * 0.05);

            // 行政消费税
            $productItem = $details->addChild('ProductItem');
            $productItem->addChild('ProductionCode', $order->inv_no);
            $productItem->addChild('Description', '綫下佣金');
            $productItem->addChild('Quantity', 1);
            $productItem->addChild('Unit', null);
            $productItem->addChild('UnitPrice', $commission + $gst);

            // $xml->asXML(resource_path('/xml/CreateInvoiceV3.xml'));

            $client = new SoapClient($this->url, [
                'trace' => true,
            ]);

            $params = [
                'invoicexml' => $xml->asXML(),
                'hastax' => 1,
                'rentid' => $this->rentid,
                'source' => $this->source,
            ];

            // return $xml;
        
            $response = $client->CreateInvoiceV3($params);

            if (strlen($response->return) != 15) {
                throw new Exception($response->return);
            }

            $order->tw_invoice = $response->return;
            $order->save();

            return $order;
        }
        catch (Exception $e) {

            return $e->getMessage();
        }
    }

    public function cancelOnlineInvoice($inv_id)
    {
        try {

            $tax_invoice = TaxInvoice::find($inv_id);

            // Creating data for params for invoice
            $data = [
                'InvoiceNumber' => strtok($tax_invoice->tw_invoice, ';'),
                'InvoiceYear' => $tax_invoice->created_at->year,
                'ReturnTaxDocumentNumber' => null,
                'Remark' => '訂單取消',
            ];

            $xml_data = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?><Invoice XSDVersion='2.8'></Invoice>");

            $xml = $this->arrayToXml($data, $xml_data);

            $client = new SoapClient($this->url, [
                'trace' => true,
            ]);

            $params = [
                'invoicexml' => $xml->asXML(),
                'rentid' => $this->rentid,
                'source' => $this->source,
            ];
        
            $response = $client->CancelInvoiceNoCheck($params);

            if ($response->return != 'C0') {
                throw new Exception($response->return);
            }

            return true;
        }
        catch (Exception $e) {

            return $e->getMessage();
        }
    }

    public function cancelOfflineInvoice($order_id)
    {
        try {

            $order = OrderOffline::find($order_id);

            // Creating data for params for invoice
            $data = [
                'InvoiceNumber' => strtok($order->tw_invoice, ';'),
                'InvoiceYear' => $order->created_at->year,
                'ReturnTaxDocumentNumber' => null,
                'Remark' => '訂單取消',
            ];

            $xml_data = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?><Invoice XSDVersion='2.8'></Invoice>");

            $xml = $this->arrayToXml($data, $xml_data);

            $client = new SoapClient($this->url, [
                'trace' => true,
            ]);

            $params = [
                'invoicexml' => $xml->asXML(),
                'rentid' => $this->rentid,
                'source' => $this->source,
            ];
        
            $response = $client->CancelInvoiceNoCheck($params);

            if ($response->return != 'C0') {
                throw new Exception($response->return);
            }

            return true;
        }
        catch (Exception $e) {

            return $e->getMessage();
        }
    }

    public function arrayToXml($data, &$xml_data) 
    {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            if( is_array($value) ) {
                $subnode = $xml_data->addChild($key);
                return $this->arrayToXml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
         }

         return $xml_data;
    }
}