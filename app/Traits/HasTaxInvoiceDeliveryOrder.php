<?php

namespace App\Traits;

use App\Models\DeliveryOrder;
use App\Models\TaxInvoice;
use Carbon\Carbon;
use PDF;

trait HasTaxInvoiceDeliveryOrder
{
    public function sendTaxInvoiceResponse($invoice, $userType, $operation, $version, $batch = false)
    {
        $param = \Request::all();
        $downloadLink = buildHttpQueryUrl($param, ['operation' => 'pdf']);
        $printLink = buildHttpQueryUrl($param, ['operation' => 'print']);

        $companies = stdObject(collect(json_decode(file_get_contents(url('/backend/js/company.json')), true))->keyBy('name'));
        $date = Carbon::create('2017','10','1','0','0','0');

        $response = [
            'userType' => $userType,
            'invoice' => $invoice,
            'version' => $version,
            'date' => $date,
            'operation' => $operation,
            'isFromApi' => $operation == 'api' ? true : false,
            'companies' => $companies,
            'batch' => $batch,
            'downloadLink' => $downloadLink,
            'printLink' => $printLink,
        ];

        switch ($operation)
        {
            case 'print':
                return view('modals.order.tax_invoice.print', $response)->render();
                break;

            case 'pdf':
                $fileName = 'Online_tax_invoice.pdf';
                $pdf = PDF::loadView('modals.order.tax_invoice.print', $response);
                return $pdf->download($fileName);
                break;

            case 'api':
                return view('modals.order.tax_invoice.print', $response)->render();
                break;

            default:
                return view('modals.order.tax_invoice.view', $response)->render();
                break;
        }
    }

    public function sendOrderDeliveriesResponse($delivery, $userType, $operation, $version, $batch = false)
    {
        $param = \Request::all();
        $downloadLink = buildHttpQueryUrl($param, ['operation' => 'pdf']);
        $printLink = buildHttpQueryUrl($param, ['operation' => 'print']);

        $response = [
            'userType' => $userType,
            'delivery' => $delivery,
            'version' => $version,
            'operation' => $operation,
            'isFromApi' => $operation == 'api' ? true : false,
            'batch' => $batch,
            'download_link' => $downloadLink,
            'print_link' => $printLink,
        ];

        switch ($operation)
        {
            case 'print':
                return view('modals.order.delivery_order.print', $response)->render();
                break;

            case 'pdf':
                $fileName = 'Online_delivery_order.pdf';
                $pdf = PDF::loadView('modals.order.delivery_order.print', $response);
                return $pdf->download($fileName);
                break;

            case 'api':
                return view('modals.order.delivery_order.print', $response)->render();
                break;

            default:
                $response['operation'] = 'view';
                return view('modals.order.delivery_order.view', $response)->render();
                break;
        }
    }

    public function sendTransactionReference($userType, $transaction, $operation, $merchant = null)
    {
        $default = $transaction->items->first();
        $currencyCode = $default->currency;
        $currencyRate = $default->currency_rate;

        $credit = round(max($transaction->items->sum('credit'), 0), 4);
        $shippingFees = round(max($transaction->items->sum('total_product_shipping_fees_credit'), 0), 4);
        $platformCharge = round(max($transaction->items->sum('cus_platform_charge_value'), 0), 4);
        $serviceCharge = round(max($transaction->items->sum('cus_service_charge_value'), 0), 4);
        $merchantCharge = round(max($transaction->items->sum('merchant_charge_vtoken'), 0), 4);
        $merchantEarn = round(max($transaction->items->sum('merchant_earn_credit'), 0), 4);
        $total = round(max($transaction->items->sum('order_vtokens'), 0), 4);

        $charges = json_decode(json_encode([
            'credit' => [
                'amount' => $credit,
                'shippingFees' => $shippingFees,
                'platformCharge' => $platformCharge,
                'serviceCharge' => $serviceCharge,
                'merchantCharge' => $merchantCharge,
                'merchantEarn' => $merchantEarn,
                'total' => $total,
            ],
            'price' => [
                'amount' => round($credit * $currencyRate, 2),
                'shippingFees' => round($shippingFees * $currencyRate, 2),
                'platformCharge' => round($platformCharge * $currencyRate, 2),
                'serviceCharge' => round($serviceCharge * $currencyRate, 2),
                'merchantCharge' => round($merchantCharge * $currencyRate, 2),
                'merchantEarn' => round($merchantEarn * $currencyRate, 2),
                'total' => round($total * $currencyRate, 2),
            ]
        ]));

        $additional = json_decode(json_encode([
            'currencyCode' => $currencyCode,
            'currencyRate' => $currencyRate,
            'platformChargeRate' => $default->cus_platform_charge_rate,
            'serviceChargeRate' => $default->cus_service_charge_rate,
            'merchantChargeRate' => $default->merchant_charge_percentage,
        ]));

        $param = [];
        $requestUrl = url()->current();
        if($userType == 'admin' && $merchant)
        {
            $param['merchant_id'] = $merchant->mer_id;
        }

        $pageData['title'] = trans('localize.transaction_reference');
        $pageData['include'] = 'modals.order.transaction_reference.data';

        $isFromApi = false;
        switch ($operation)
        {
            case 'print':
                $pageData = json_decode(json_encode($pageData));
                return view('modals.order.transaction_reference.print', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'))->render();
                break;

            case 'pdf':
                $pageData = json_decode(json_encode($pageData));
                $fileName = 'Transaction_reference-'.$transaction->transaction_id.'.pdf';
                $pdf = PDF::loadView('modals.order.transaction_reference.print', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'));
                return $pdf->download($fileName);
                break;

            case 'api':
                $isFromApi = true;
                $pageData = json_decode(json_encode($pageData));
                return view('modals.order.transaction_reference.print', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'))->render();
                break;

            default:
                $param['operation'] = 'print';
                $printLink = $requestUrl.'?'.http_build_query($param);
                $param['operation'] = 'pdf';
                $downloadLink = $requestUrl.'?'.http_build_query($param);

                $pageData['printLink'] = $printLink;
                $pageData['downloadLink'] = $downloadLink;

                $pageData = json_decode(json_encode($pageData));
                return view('modals.order.transaction_reference.view', compact('charges', 'transaction', 'additional', 'merchant', 'pageData', 'isFromApi'))->render();
                break;
        }
    }

    public function orderDeliveryV1($delivery, $userType, $operation, $batch = false)
    {

    }

    public function orderDeliveryV2($delivery, $userType, $operation, $batch = false)
    {
        $param = \Request::all();
        $requestUrl = url()->current();

        $param['operation'] = 'pdf';
        $downloadLink = $requestUrl.'?'.http_build_query($param);
        $param['operation'] = 'print';
        $printLink = $requestUrl.'?'.http_build_query($param);

        $isFromApi = false;
        $response = [
            'userType' => $userType,
            'delivery' => $delivery,
            'operation' => $operation,
            'isFromApi' => $isFromApi,
            'batch' => $batch,
            'download_link' => $downloadLink,
            'print_link' => $printLink,
        ];

        switch ($operation)
        {
            case 'print':
                return view('modals.order.V2.statement.print', $response)->render();
                break;

            case 'pdf':
                $fileName = 'Online_delivery_order.pdf';
                $pdf = PDF::loadView('modals.order.V2.statement.print', $response);
                return $pdf->download($fileName);
                break;

            case 'api':
                $response['isFromApi'] = true;
                return view('modals.order.V2.statement.print', $response)->render();
                break;

            default:
                $response['operation'] = 'view';
                return view('modals.order.V2.statement.view', $response)->render();
                break;
        }
    }
}