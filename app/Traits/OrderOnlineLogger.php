<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Models\ParentOrder;
use Auth;
use App\Models\OrderOnlineLog;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Merchant;
use App\Models\Admin;
use Mockery\CountValidator\Exception;

trait OrderOnlineLogger
{
    public static function log($parent_order_id = null, $order_id = null, $user_id = null, $user_type, $description, $remarks = null)
    {

        try
        {
            if(!$parent_order_id && !$order_id)
            {
                return false;
            }

            if($parent_order_id)
            {
                $parent = ParentOrder::find($parent_order_id);
                if(!$parent)
                {
                    return false;
                }

                $orders = $parent->items;
                if(is_array($order_id) && !empty($order_id))
                {
                    $orders = $orders->whereIn('order_id', $order_id);
                }
            }
            else
            {
                if(!is_array($order_id))
                {
                    $order_id = [$order_id];
                }

                $orders = Order::whereIn('order_id', $order_id)->get();
            }

            if($orders->isEmpty())
            {
                return false;
            }

            switch ($user_type)
            {
                case 'system':
                    $user_id = 0;
                    $user_type = 0;
                    break;

                case 'customer':

                    $user_type = 1;

                    if(is_null($user_id) && !Auth::check())
                    {
                        return false;
                    }

                    if(!is_null($user_id))
                    {
                        $user = Customer::find($user_id);
                        if(!$user)
                        {
                            return false;
                        }

                        $user_id = $user->cus_id;
                    }
                    else
                    {
                        $user_id = Auth::user()->cus_id;
                    }
                    break;

                case 'merchant':

                    $user_type = 2;

                    if(is_null($user_id) && !Auth::guard('merchants')->check())
                    {
                        return false;
                    }

                    if(!is_null($user_id))
                    {
                        $user = Merchant::find($user_id);
                        if(!$user)
                        {
                            return false;
                        }

                        $user_id = $user->mer_id;
                    }
                    else
                    {
                        $user_id = Auth::guard('merchants')->user()->mer_id;
                    }
                    break;

                case 'admin':

                    $user_type = 3;

                    if(is_null($user_id) && !Auth::guard('admins')->check())
                    {
                        return false;
                    }

                    if(!is_null($user_id))
                    {
                        $user = Admin::find($user_id);
                        if(!$user)
                        {
                            return false;
                        }

                        $user_id = $user->adm_id;
                    }
                    else
                    {
                        $user_id = Auth::guard('admins')->user()->adm_id;
                    }
                    break;

                default:
                    return false;
                    break;
            }

            $now = Carbon::now('UTC');
            foreach ($orders as $order)
            {
                $log = new OrderOnlineLog;
                $log->parent_order_id = $order->parent_order_id;
                $log->order_id = $order->order_id;
                $log->user_id = $user_id;
                $log->user_type = $user_type;
                $log->description = $description;
                $log->remarks = $remarks;
                $log->created_at = $now;
                $log->updated_at = $now;
                $log->save();
            }

            return true;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
}