<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;
use App\Repositories\OrderRepo;
use App\Repositories\StoreRepo;
use App\Repositories\TransactionRepo;
use App\Repositories\FundRepo;
use App\Repositories\MerchantRepo;
use App\Repositories\OrderOfflineRepo;
use App\Repositories\ProductRepo;
use App\Repositories\ProductPricingRepo;
use App\Repositories\CountryRepo;
use App\Repositories\GeneratedCodeRepo;
use App\Repositories\LimitRepo;
use App\Repositories\CourierRepo;
use App\Repositories\InvoiceRepo;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Carbon\Carbon;
use Helper;

class TransactionController extends Controller
{
    public function __construct(Mailer $mailer) {
        $this->mailer = $mailer;
        $this->permissions = [];
    }

    public function product_orders($operation)
    {
        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);
        $export_permission = in_array('transactiononlineorderslistexport', $admin_permission);
        $accept_order_permission = in_array('transactiononlineacceptorder', $admin_permission);
        $update_shipping_info_permission = in_array('transactiononlineupdateshippinginfo', $admin_permission);
        $cancel_order_permission = in_array('transactiononlinecancelorder', $admin_permission);
        $order_refund_permission = in_array('transactiononlineorderrefund', $admin_permission);
        $redeem_coupons_permission = in_array('transactionredeemcoupons', $admin_permission);
        $cancel_coupons_permission = in_array('transactioncancelcoupons', $admin_permission);
        $admin_country_id_list = Controller::getAdminCountryIdList($adm_id);
        $accept_multiple_order_permission = in_array('transactiononlineacceptmultipleorder', $admin_permission);

		if(in_array('transactiononlineorderslist', $admin_permission) == false){
		return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
		}

        $mer_id = 'all';

        $input = \Request::only('id', 'name', 'status', 'sort', 'tid', 'cid', 'pid', 'mid', 'start', 'end', 'merchant_countries', 'customer_countries', 'sid', 'oid');

        $status_list = [
            ''  => trans('localize.all'),
            '2' => trans('localize.pending'),
            '4' => trans('localize.completed'),
            '5' => trans('localize.cancelled'),
            '6' => trans('localize.refunded'),
        ];

        $type_list = [
            1 => ['name' => 'orders', 'lang' => trans('localize.Online_Orders')],
            3 => ['name' => 'coupons', 'lang' => trans('localize.coupon_orders')],
            4 => ['name' => 'tickets', 'lang' => trans('localize.ticket_orders')],
            5 => ['name' => 'ecards', 'lang' => trans('localize.e-card.orders')],
        ];

        switch ($operation) {
            case 'orders':
                $status_list = [
                    ''  => trans('localize.all'),
                    '1' => trans('localize.processing'),
                    '2' => trans('localize.packaging'),
                    '3' => trans('localize.shipped'),
                    '4' => trans('localize.completed'),
                    '5' => trans('localize.cancelled'),
                    '6' => trans('localize.refunded'),
                ];
                $type = 1;
                break;

            case 'coupons':
                $type = 3;
                break;

            // case 'tickets':
            //     $type = 4;
            //     break;

            case 'ecards':
                $type = 5;
                break;

            default:
                return back()->with('error', 'Invalid Transaction');
                break;
        }

        $countries = CountryRepo::get_countries($admin_country_id_list);
        $orders = OrderRepo::get_orders_by_status($mer_id, $type, $input);
        $charges = OrderRepo::get_online_total_transaction($mer_id, $type, $input);
		return view('admin.transaction.product.order', compact('orders', 'status_list','input','charges', 'countries', 'type', 'type_list','cancel_coupons_permission','redeem_coupons_permission','order_refund_permission','cancel_order_permission','update_shipping_info_permission', 'accept_order_permission','export_permission','accept_multiple_order_permission'));
    }

    public function product_cod()
    {
        $input = (isset($data['search'])) ? $data['search'] : '';
        $status = (isset($_GET['s'])) ? $_GET['s'] : '';
        $status_list = array(
            ''  => trans('localize.all'),
            '1' => trans('localize.Success'),
            '2' => trans('localize.completed'),
            '3' => trans('localize.Hold'),
            '4' => trans('localize.Failed'),
        );

        $coddetails = TransactionRepo::get_product_orders_cod($status,$input);
        return view('admin.transaction.product.cod', compact('coddetails','status_list','status'));
    }

    public function deal_orders()
    {
        $mer_id = 'all';
        $status = (isset($_GET['s'])) ? $_GET['s'] : '';
        $status_list = array(
            ''  => trans('localize.all'),
            '1' => trans('localize.Success'),
            '2' => trans('localize.completed'),
            '3' => trans('localize.Hold'),
            '4' => trans('localize.Failed'),
        );

        $input = (isset($data['search'])) ? $data['search'] : '';
        $deals = OrderRepo::get_all_deal_orders_by_status($status, $mer_id, $input);

		return view('admin.transaction.deal.order', compact('deals','status_list','status'));
    }

    public function deal_cod()
    {
        $status = (isset($_GET['s'])) ? $_GET['s'] : '';
        $status_list = array(
            ''  => trans('localize.all'),
            '1' => trans('localize.Success'),
            '2' => trans('localize.completed'),
            '3' => trans('localize.Hold'),
            '4' => trans('localize.Failed'),
        );
        $input = (isset($data['search'])) ? $data['search'] : '';

        $coddetails = TransactionRepo::get_deal_orders_cod($status,$input);

		return view('admin.transaction.deal.cod', compact('coddetails','status_list','status'));
    }


    public function update_order_cod()
	{
        $orderid = $_GET['order_id'];
		$status = $_GET['status'];

		$updaters = TransactionRepo::update_cod_status($status, $orderid);
		if($updaters)
		{
			echo trans('localize.Success');
		}
	}

    public function fund_request()
    {
        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);
        $admin_country_id_list = Controller::getAdminCountryIdList($adm_id);
        $export_permission = in_array('transactionfundrequestlistexport', $admin_permission);
        $fund_paid = in_array('transactionfundrequesfundpaid', $admin_permission);
        $fund_approval_permission = in_array('transactionfundrequesapproval', $admin_permission);

        $input = \Request::only('id', 'start', 'end', 'type', 'status', 'sort','countries','name','email');
        $status= $input['status'];
        $status_list = array(
            ''  => trans('localize.all'),
            '0' => trans('localize.pending'),
            '1' => trans('localize.approved'),
            '2' => trans('localize.declined'),
            '3' => trans('localize.paid'),
        );

		$input['admin_country_id_list'] = $admin_country_id_list;

        $funds = FundRepo::get_funds_by_status($input);
        $countries = CountryRepo::get_countries($admin_country_id_list);
        return view('admin.transaction.fund.manage', compact('fund_approval_permission','fund_paid','export_permission','funds','status_list','input','status','countries'));
    }

    public function update_fund_withdraw_status($wd_id, $status)
    {
        $fund = FundRepo::get_fund($wd_id);
        if ($fund->wd_status == 2)
            return back()->with('error', trans('localize.Fund_Withdraw_status_already_been_declined'));

         if ($fund->wd_status == 3)
            return back()->with('error', trans('localize.Fund_Withdraw_status_already_been_paid'));

        // Double check just in case
        $check_merchant_log = MerchantRepo::get_merchant_vtoken_log_by_withdraw_id($wd_id);
        if (!empty($check_merchant_log))
            return back()->with('error', trans('localize.Fund_already_withdrawed'));

        $update_fund = FundRepo::update_fund_status($wd_id, $status);

        if($update_fund)
        {
            if($status == 3) {
                $fund = FundRepo::get_fund($wd_id);
                $mer_id = $fund->wd_mer_id;

                $merchant = MerchantRepo::get_merchant($mer_id);

                $credit_debit = $fund->wd_submited_wd_amt + $fund->wd_admin_comm_amt;
                $current_merchant_credit = $merchant->mer_vtoken;

                $new_merchant_credit = $current_merchant_credit - $credit_debit;

                $remark = "Withdrawal";
                $log_credit = MerchantRepo::update_merchant_credit($mer_id, $wd_id, $credit_debit, $new_merchant_credit, $remark);

                $data = [
                    'bank_acc_name' => $merchant->bank_acc_name,
                    'bank_acc_no' => $merchant->bank_acc_no,
                    'bank_name' => $merchant->bank_name,
                    'receipt_no' => $fund->wd_id,
                    'payment_date' => date('d F Y'),
                    'withdraw_by' => $merchant->mer_fname . ' ' . $merchant->mer_lname,
                    'withdraw_date' => \Helper::UTCtoTZ($fund->created_at),
                    'approve_by' => 'MeiHome Administrator',
                    'credit' => $fund->wd_submited_wd_amt,
                    'currency' => $fund->wd_rate,
                    'currency_code' => $fund->wd_currency,
                    'amount' =>number_format($fund->wd_submited_wd_amt * $fund->wd_rate, 2)
                ];

                $this->mailer->send('front.emails.withdraw_request_slip', $data, function (Message $m) use ($merchant) {
                    $m->to('operation@meihome.asia', 'MeiHome Operation')->subject('MeiHome Official Payment Slip');
                });
            }
            return back()->with('success', trans('localize.Successfully_update_fund_withdraw'));
        }
        return back()->with('error', trans('localize.Failed_to_update_fund_withdraw'));
    }

    public function order_offline()
    {
        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_permission = Controller::adminPermissionList($adm_id);
        $export_permission = in_array('transactionofflineorderslistexport', $admin_permission);
        $order_refund_permission = in_array('transactionofflineorderrefund', $admin_permission);
        $admin_country_id_list = Controller::getAdminCountryIdList($adm_id);

		if(in_array('transactionofflineorderslist', $admin_permission) == false){
		return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
		}

        $mer_id = 'all'; // For Admin | calling from the same repo as merchant site.
        $input = \Request::only('id', 'cid', 'mid', 'search', 'start', 'end', 'type', 'status', 'sort', 'merchant_countries', 'customer_countries', 'sid','tax_inv_no');
        $status= $input['status'];
        $status_list = array(
            ''  => trans('localize.all'),
            '0' => trans('localize.unpaid'),
            '1' => trans('localize.paid'),
            '2' => trans('localize.cancelbymember'),
            '3' => trans('localize.cancelbymerchant'),
            '4' => trans('localize.refunded'),
        );

        $countries = CountryRepo::get_countries($admin_country_id_list);
        $input['admin_countries'] = $admin_country_id_list;

        $orders = OrderOfflineRepo::get_orders_offline($mer_id, $input);
        $total = OrderOfflineRepo::get_grand_total($mer_id, $input);



        return view('admin.transaction.order.offline', compact('orders', 'status', 'status_list', 'input', 'total', 'countries', 'order_refund_permission', 'export_permission'));
    }

    public function refund_online_order($order_id, $operation)
    {
        $order = OrderRepo::get_order_by_id($order_id);
        if(!$order || $order->order_type == 5)
            return back()->with('error', trans('localize.invalid_operation'));

        $order_status = $order->order_status;

        if($order_status == 4) {
            // old : order_vtokens - merchant_charge_token
            $merchant_amount = ROUND(($order->order_vtokens - $order->merchant_charge_vtoken), 4);

            if ($order->cus_platform_charge_value > 0 && $order->cus_service_charge_value > 0) {
                // new : ((product_original_price * order_qty) / currency_rate) - merchant_charge_token
                $merchant_amount = ROUND(((($order->product_original_price * $order->order_qty) / $order->currency_rate) - $order->merchant_charge_vtoken), 4);
            }

            if ($order->mer_vtoken < $merchant_amount)
                return redirect('admin/transaction/product/orders')->with('error', trans('localize.Error_while_refund_this_orders'));
        }

        $results = \DB::select("CALL refund_online_order($order_id)");

        if ($results) {
            // update pricing quantity
            ProductPricingRepo::refund_pricing_quantity($order_id, $operation);

            // deduct limit transaction
            LimitRepo::deduct_limit_transactions('online', $order_id);

            //cancel coupon
            if(in_array($order->order_type, [3,4,5]))
                $cancel = GeneratedCodeRepo::cancel_code_by_order($order_id);

            return back()->with('success', trans('localize.Successfully') . ' ' . $operation . ' ' . trans('localize.order') . '!');
        } else {
            return back()->with('error', trans('localize.Error_while_update_orders'));
        }
    }

    public function refund_offline_order($order_id)
    {
        $order = OrderOfflineRepo::get_order_offline_by_id($order_id);

        $merchant_amount = ROUND(($order->v_token - $order->merchant_charge_token), 4);
        if ($order->mer_vtoken < $merchant_amount)
            return redirect('admin/transaction/offline')->with('error', trans('localize.Error_while_refund_this_orders'));

        $results = \DB::select("CALL refund_offline_order($order_id,'')");

        if ($results) {

            LimitRepo::deduct_limit_transactions('offline', $order_id);

            return redirect('admin/transaction/offline')->with('success', trans('localize.Successfully_refund_order'));
        } else {
            return redirect('admin/transaction/offline')->with('error', trans('localize.Error_while_refund_this_order'));
        }
    }

    public function update_batch_transaction($operation)
    {
        if(\Request::isMethod('post'))
        {
            $data = \Request::all();

            if(!isset($data['order_id']))
                return back()->withError('Nothing to be updated');

            OrderRepo::update_batch_status_transaction($operation, $data['order_id']);

            return back()->withSuccess(trans('localize.recordupdated'));
        }
    }

    public function cancel_code($type, $order_id, $serial_number)
    {
        switch ($type) {
            case 'coupon':

                $coupon = GeneratedCodeRepo::find_coupon($serial_number);
                if(!$coupon || $coupon->order_id != $order_id || $coupon->status == 3)
                    return back()->with('error', trans('localize.Invalid_coupon_code') . '!');

                $cancel = GeneratedCodeRepo::cancel_code($type, $serial_number);
                if($cancel)
                    return back()->with('success', trans('localize.Coupon_has_been_canceled') . '!');

                break;

            case 'ticket':

                $ticket = GeneratedCodeRepo::find_ticket($serial_number);
                if(!$ticket || $ticket->order_id != $order_id || $ticket->status == 3)
                    return back()->with('error', trans('localize.Invalid_coupon_code') . '!');

                $cancel = GeneratedCodeRepo::cancel_code($type, $serial_number);
                if($cancel)
                    return back()->with('success', trans('localize.Ticket_has_been_canceled') . '!');

                break;
        }

        return back()->with('error', 'Internal server error!');
    }

    public function completing_merchant_order($order_id)
    {
        $order = OrderRepo::get_order_by_id($order_id);
        if(!$order || $order->order_status != 3 || $order->product_shipping_fees_type != 3) {
            return 0;
        }

        if(\Request::isMethod('post'))
        {
            $data = \Request::all();
            \Validator::make($data, [
                'remarks' => 'required',
            ])->validate();

            $update = OrderRepo::completing_merchant_order($order_id, $data['remarks']);
            if($update)
                return back()->with('success', trans('localize.recordupdated'));

            return back()->with('error', trans('localize.internal_server_error.title'));
        }

        return view('modals.product_received', compact('order'))->render();
    }

    public function online_orders()
    {
        $admin = \Auth::guard('admins')->user();
        $input = \Request::only('transaction_id', 'customer_id', 'merchant_id', 'start_date', 'end_date', 'status', 'sort', 'merchant_countries', 'customer_countries');

        \Validator::make($input, [
            'merchant_id' => 'nullable|integer',
            'customer_id' => 'nullable|integer',
            'sort' => 'nullable|in:old,new',
            'start_date' => 'nullable|date_format:"d/m/Y"',
            'end_date' => 'nullable|date_format:"d/m/Y"',
            'merchant_countries' => 'nullable|array',
            'customer_countries' => 'nullable|array',
            'merchant_countries.*' => 'integer',
            'customer_countries.*' => 'integer',
        ])->validate();

        $admin_countries = Controller::getAdminCountryIdList($admin->adm_id);
        $input['admin_countries'] = $admin_countries;

        $transactions = OrderRepo::get_online_orders($input);
        $total = $this->get_online_orders_charges($transactions);

        $permissions = Controller::adminPermissionList($admin->adm_id);
        $countries = CountryRepo::get_countries($admin_countries);

        $status_list = [
            '1' => trans('localize.processing'),
            '2' => implode(' / ', [trans('localize.packaging'), trans('localize.pending')]),
            '3' => trans('localize.shipped'),
            '4' => trans('localize.completed'),
            '5' => trans('localize.cancelled'),
            '6' => trans('localize.refunded'),
        ];

        return view('admin.transaction.order.online.listing', compact('transactions', 'input', 'countries', 'total', 'permissions', 'status_list'));
    }

    public function transaction_detail($parent_id, $merchant_id)
    {
        $admin = \Auth::guard('admins')->user();
        $this->permissions = Controller::adminPermissionList($admin->adm_id);

		if(!Helper::adminPermission('transactiononlineorderslist', $this->permissions)){
		    return redirect('admin')->with('denied', trans('localize.You_are_not_authorized_to_access_that_page'));
        }

        $transaction = OrderRepo::get_online_order_items($parent_id, $merchant_id);
        if(!$transaction || $transaction->invoices->isEmpty())
        {
            return redirect('/admin')->with('error', 'Invalid transaction');
        }

        $couriers = CourierRepo::get_couriers();

        $invoices = $transaction->invoices->map(function ($item) {
            $item->tax_number_full = ($item->version == 1 || $item->version == 5) ? $item->tax_number('ONS') : $item->tax_number('OMP');
            return $item;
        });
        $deliveries = $this->get_invoice_delivery_orders($invoices);
        $invoice = $invoices->first();

        $shipping = $transaction->shipping_address? $transaction->shipping_address : null;
        $customer = $invoice->customer? $invoice->customer : null;
        $merchant = $invoice->merchant? $invoice->merchant : null;

        $render = json_decode(json_encode($this->render_order_items($invoices)));
        $orders = $render->items;
        $actions = $render->actions;
        $displayInvoice = $render->displayInvoice;
        $logs = $transaction->logs->whereIn('order_id', $render->orders_id);

        $redemptions = $invoice->version == 3 ? true : false;

        return view('admin.transaction.order.online.detail.view', compact('customer', 'shipping', 'merchant', 'transaction', 'orders', 'couriers', 'actions', 'invoices', 'deliveries', 'displayInvoice', 'permissions', 'logs', 'redemptions'));
    }

    protected function render_order_items($invoices)
    {
        $items = collect([]);
        $actions = collect([]);
        $displayInvoice = true;
        $ids = collect([]);

        if(!$invoices)
            return $new;

        foreach ($invoices as $invoice) {

            foreach ($invoice->items as $item) {

                $product = null;
                if($item->product)
                {
                    $product = [
                        'id' => $item->product->pro_id,
                        'name' => $item->product->title,
                    ];
                }

                $ids->push($item->order_id);

                $items->push([
                    'order_type' => $item->order_type,
                    'order_id' => $item->order_id,
                    'currency_code' => $item->currency,
                    'currency_rate' => $item->currency_rate,
                    'quantity' => $item->order_qty,
                    'status' => [
                        'code' => $item->order_status,
                        'text' => $item->status(),
                    ],
                    'grouping' => $invoice->item_type == 2? 0 : $invoice->shipment_type,
                    'shipping_type' => [
                        'code' => $item->product_shipping_fees_type,
                        'text' => $item->shipping_type(),
                    ],
                    'total' => $item->unrounded_total(),
                    'product' => $product,
                    'options' => !empty($item->order_attributes)? json_decode($item->order_attributes) : null,
                ]);

                switch ($item->order_status) {

                    case 1:
                        $displayInvoice = false;
                        if(!$actions->has('accept_order') && Helper::adminPermission('transactiononlineacceptorder', $this->permissions))
                        {
                            $actions->put('accept_order', ['text' => trans('localize.Order.accept'), 'button' => 'btn-primary', 'order' => 1]);
                        }
                        break;

                    case 2:
                        // only create delivery order for physical product
                        if($invoice->item_type == 1 && Helper::adminPermission('transactiononlineupdateshippinginfo', $this->permissions))
                        {
                            if(!$actions->has('create_shipment') && in_array($invoice->shipment_type, [1]))
                            {
                                $actions->put('create_shipment', ['text' => trans('localize.Order.arrange.shipment'), 'button' => 'btn-info', 'order' => 2]);
                            }

                            if(!$actions->has('arrange_pickup') && in_array($invoice->shipment_type, [2]))
                            {
                                $actions->put('arrange_pickup', ['text' => trans('localize.Order.arrange.self_pickup'), 'button' => 'btn-info', 'order' => 3]);
                            }
                        }
                        break;

                    case 3:
                        if($invoice->item_type == 1 && Helper::adminPermission('transactiononlineacceptorder', $this->permissions))
                        {
                            if($invoice->shipment_type == 2 && !$actions->has('complete_order'))
                            {
                                $actions->put('complete_order', ['text' => trans('localize.Order.complete'), 'button' => 'btn-default', 'order' => 4]);
                            }
                        }
                        break;

                    case 4:
                        if($invoice->item_type == 1 && Helper::adminPermission('transactiononlineorderrefund', $this->permissions))
                        {
                            if(!$actions->has('refund_order'))
                            {
                                $actions->put('refund_order', ['text' => trans('localize.Order.refund'), 'button' => 'btn-warning', 'order' => 5]);
                            }
                        }
                        break;
                }

                if(in_array($item->order_status, [1,2,3]) && in_array($item->order_type, [1,2]) && !$actions->has('cancel_order') && Helper::adminPermission('transactiononlinecancelorder', $this->permissions))
                {
                    $actions->put('cancel_order', ['text' => trans('localize.Order.cancel'), 'button' => 'btn-danger', 'order' => 6]);
                }
            }
        }

        if(!$displayInvoice)
        {
            if($actions->has('create_shipment'))
            {
                $actions->pull('create_shipment');
            }

            if($actions->has('arrange_pickup'))
            {
                $actions->pull('arrange_pickup');
            }
        }

        return [
            'items' => $items->sortByDesc('grouping')->groupBy('grouping')->toArray(),
            'actions' => $actions->sortBy('order'),
            'displayInvoice' => $displayInvoice,
            'orders_id' => $ids,
        ];
    }

    public function order_status_update($parent_id, $merchant_id)
    {
        $admin = \Auth::guard('admins')->user();
        $this->permissions = Controller::adminPermissionList($admin->adm_id);

        $data = \Request::all();
        $action = isset($data['action'])? $data['action'] : null;

        $validate = [
            'action' => 'required|in:accept_order,create_shipment,arrange_pickup,cancel_order,refund_order,complete_order',
            // 'order_id' => 'required|array',
            // 'order_id.*' => 'integer',
            'courier_id' => in_array($action , ['create_shipment'])? 'required|integer' : '',
            'tracking_number' => 'required_if:action,create_shipment',
            'appointment_detail' => 'required_if:action,arrange_pickup',
            'remarks' => 'required_if:action,complete_order',
        ];

        // if(in_array($action, ['create_shipment', 'arrange_pickup']))
        // {
        //     unset($validate['order_id'], $validate['order_id.*']);
        // }

        \Validator::make($data, $validate, [
            'order_id.*.integer' => trans('validation.integer', ['attribute' => 'order id']),
            'tracking_number.required_if' => trans('validation.required'),
            'appointment_detail.required_if' => trans('validation.required')
        ])->validate();

        $validate = false;
        $merchant = MerchantRepo::find_merchant($merchant_id);

        if(isset($data['order_id']))
        {
            $orders_id = array_map('intval', $data['order_id']);
            $validate = OrderRepo::update_transaction_status_order($action, $parent_id, $merchant_id, $orders_id);
        }
        else
        {
            $orders_id = OrderRepo::update_transaction_status_order($action, $parent_id, $merchant_id);
            $validate = !$orders_id? false : true;
        }

        if(!$validate || !$merchant)
            return back()->with('error', 'Unable to update order');

        // data validation
        $access = true;
        switch ($action) {
            case 'accept_order':
                if(!Helper::adminPermission('transactiononlineacceptorder', $this->permissions))
                {
                    $access = false;
                }
                break;

            case 'complete_order':
                if(!Helper::adminPermission('transactiononlineacceptorder', $this->permissions))
                {
                    $access = false;
                }

                $order_id = reset($orders_id);
                $deliveryOrder = OrderRepo::find_delivery_order($order_id);
                if(!$deliveryOrder)
                    return back()->with('error', 'Unable to update order');

                $data['delivery_order_id'] = $deliveryOrder->id;
                break;

            case 'create_shipment':
            case 'arrange_pickup':
                if(!Helper::adminPermission('transactiononlineupdateshippinginfo', $this->permissions))
                {
                    $access = false;
                }

                $taxInvoice = OrderRepo::find_orders_invoice($orders_id);
                if(!$taxInvoice)
                    return back()->with('error', 'Unable to update order');

                $data['invoice_id'] = $taxInvoice->id;
                break;

            case 'cancel_order':
                if(!Helper::adminPermission('transactiononlinecancelorder', $this->permissions))
                {
                    $access = false;
                }
                break;

            case 'refund_order':
                if(!Helper::adminPermission('transactiononlineorderrefund', $this->permissions))
                {
                    $access = false;
                }

                $orders = OrderRepo::get_orders_by_id($orders_id);
                $deductFromMerchant = ROUND($orders->sum('merchant_earn'), 4);
                if ($merchant->mer_vtoken < $deductFromMerchant)
                    return redirect()->with('error', 'Insufficient merchant credit');
                break;
        }

        if(!$access)
        {
            return back()->with('error', trans('localize.unauthorized_access'));
        }

        $update = OrderRepo::update_batch_status_transaction($action, $orders_id, $data);
        if(!$update)
            return back()->with('error', 'Unable to update order');

        return back()->with('success', 'Order has been update');
    }

    protected function get_invoice_delivery_orders($invoices)
    {
        $deliveries = collect([]);
        foreach ($invoices as $invoice) {
            foreach ($invoice->deliveries as $delivery) {
                $deliveries->push($delivery);
            }
        }

        return $deliveries;
    }

    public function online_invoices()
    {
        $input = \Request::only(['transaction_id', 'tax_number', 'merchant_id', 'customer_id', 'item_type', 'shipment_type', 'sort']);
        \Validator::make($input, [
            'merchant_id' => 'nullable|integer',
            'customer_id' => 'nullable|integer',
            'item_type' => 'nullable|integer',
            'shipment_type' => 'nullable|integer',
            'sort' => 'nullable|in:old,new',
        ])->validate();

        $item_types = json_decode(json_encode([
            '' => trans('localize.all'),
            1 => trans('localize.normal_product'),
            2 => trans('localize.virtual_product')
        ]));

        $shipment_types = json_decode(json_encode([
            '' => trans('localize.all'),
            0 => trans('localize.not_assigned'),
            1 => implode(', ', [trans('localize.shipping_fees_free'), trans('localize.shipping_fees_product'), trans('localize.shipping_fees_transaction')]),
            2 => trans('localize.self_pickup')
        ]));

        $invoices = InvoiceRepo::get_online_invoices($input);

        return view('admin.transaction.order.online.invoices', compact('invoices', 'input', 'item_types', 'shipment_types'));
    }

    public function online_deliveries()
    {
        $redemptions = false;
        $url = explode('/', url()->current());
        if(end($url) == 'redemptions')
        {
            $redemptions = true;
        }

        $input = \Request::only(['tax_number', 'do_number', 'merchant_id', 'customer_id', 'shipment_type', 'sort']);

        $adm_id = \Auth::guard('admins')->user()->adm_id;
        $admin_countries = Controller::getAdminCountryIdList($adm_id);
        $input['admin_countries'] = $admin_countries;

        \Validator::make($input, [
            'merchant_id' => 'nullable|integer',
            'customer_id' => 'nullable|integer',
            'sort' => 'nullable|in:old,new',
        ])->validate();

        $shipment_types = json_decode(json_encode([
            '' => trans('localize.all'),
            0 => trans('localize.not_assigned'),
            1 => trans('localize.by_courier'),
            2 => trans('localize.self_pickup')
        ]));

        $input['redemptions'] = $redemptions;
        $deliveries = InvoiceRepo::get_online_deliveries($input);

        return view('admin.transaction.order.online.deliveries', compact('deliveries', 'input', 'shipment_types', 'redemptions'));
    }

    protected function get_online_orders_charges($transactions, $byParent = true)
    {
        $total = [
            'order_price' => 0,
            'order_credit' => 0,
            'platform_charge_credit' => 0,
            'service_charge_credit' => 0,
            'merchant_charge_credit' => 0,
            'shipping_fees_credit' => 0,
            'merchant_earn_credit' => 0,
        ];

        if($byParent)
        {
            foreach ($transactions as $transaction) {
                $total['order_price'] += $transaction->items->sum('order_price');
                $total['order_credit'] += $transaction->items->sum('order_credit');
                $total['platform_charge_credit'] += $transaction->items->sum('platform_charge_credit');
                $total['service_charge_credit'] += $transaction->items->sum('service_charge_credit');
                $total['merchant_charge_credit'] += $transaction->items->sum('merchant_charge_credit');
                $total['shipping_fees_credit'] += $transaction->items->sum('shipping_fees_credit');
                $total['merchant_earn_credit'] += $transaction->items->sum('merchant_earn_credit');
            }
        } else {
            $total['order_price'] += $transactions->sum('order_price');
            $total['order_credit'] += $transactions->sum('order_credit');
            $total['platform_charge_credit'] += $transactions->sum('platform_charge_credit');
            $total['service_charge_credit'] += $transactions->sum('service_charge_credit');
            $total['merchant_charge_credit'] += $transactions->sum('merchant_charge_credit');
            $total['shipping_fees_credit'] += $transactions->sum('shipping_fees_credit');
            $total['merchant_earn_credit'] += $transactions->sum('merchant_earn_credit');
        }

        return json_decode(json_encode($total));
    }

    public function online_histories()
    {
        $admin = \Auth::guard('admins')->user();
        $input = \Request::only('order_id', 'transaction_id', 'product_id', 'customer_id', 'merchant_id', 'store_id', 'do_number', 'product_name', 'sku_code', 'start_date', 'end_date', 'type', 'status', 'sort', 'merchant_countries', 'customer_countries');

        \Validator::make($input, [
            'order_id' => 'nullable|integer',
            'transaction_id' => 'nullable',
            'product_id' => 'nullable|integer',
            'customer_id' => 'nullable|integer',
            'merchant_id' => 'nullable|integer',
            'store_id' => 'nullable|integer',
            'sort' => 'nullable|in:old,new',
            'start_date' => 'nullable|date_format:"d/m/Y"',
            'end_date' => 'nullable|date_format:"d/m/Y"',
            'merchant_countries' => 'nullable|array',
            'customer_countries' => 'nullable|array',
            'merchant_countries.*' => 'integer',
            'customer_countries.*' => 'integer',
        ])->validate();

        $admin_countries = Controller::getAdminCountryIdList($admin->adm_id);
        $input['admin_countries'] = $admin_countries;

        $orders = OrderRepo::online_histories($input);
        $total = $this->get_online_orders_charges($orders, false);

        $permissions = Controller::adminPermissionList($admin->adm_id);
        $countries = CountryRepo::get_countries($admin_countries);

        $types = [
            '1' => trans('localize.normal_product'),
            '3' => trans('localize.coupon'),
            // '4' => trans('localize.ticket'),
            '5' => trans('localize.e-card.name'),
        ];

        $status_list = [
            '1' => trans('localize.processing'),
            '2' => trans('localize.packaging'),
            '3' => trans('localize.shipped'),
            '4' => trans('localize.completed'),
            '5' => trans('localize.cancelled'),
            '6' => trans('localize.refunded'),
        ];

        return view('admin.transaction.order.online.histories', compact('orders', 'input', 'countries', 'total', 'permissions', 'types', 'status_list'));
    }
}
