$(document).ready(function ()
{
    $('[data-toggle="tooltip"]').tooltip();

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    var pageInput = document.getElementById("pageno");
    if(pageInput)
    {
        var max = $(pageInput).attr('max');
        if(typeof max !== typeof undefined && max !== false)
        {
            $(pageInput).change(function(){
                var input = $(this).val();
                if(input > max){
                    $(this).val(max);
                }
            });
        }
    }
});

function gotopage($page)
{
    $val = $("#pageno").val();

    var href = window.location.href.substring(0, window.location.href.indexOf('?'));
    var qs = window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.length);
    var newParam = $page + '=' + $val;

    if (qs.indexOf($page + '=') == -1) {
        if (qs == '') {
            qs = '?'
        }
        else {
            qs = qs + '&'
        }
        qs = newParam;

    }
    else {
        var start = qs.indexOf($page + "=");
        var end = qs.indexOf("&", start);
        if (end == -1) {
            end = qs.length;
        }
        var curParam = qs.substring(start, end);
        qs = qs.replace(curParam, newParam);
    }
    window.location.replace(href + '?' + qs);
}
