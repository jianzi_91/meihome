@extends('layouts.web.master')

@section('header')
	@include('layouts.web.header.main')
@endsection

@section('content')
<div class="ListingTopbar">
    <h4 class="ListingCategory">@lang('localize.updateAccount')</h4>
</div>

<div class="ContentWrapper">
    <div class="login-selection">
        <div class="alert alert-danger">
            @lang('localize.required_before_shopping')
        </div>
    </div>

    @include('layouts.partials.status')

    <form id="update_info" role="form" method="POST" action="{{ url('profile/update') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label class="control-label">@lang('localize.email') <span style="color:red;">*</span></label>
            <input type="text" name="email" class="form-control compulsary" placeholder="@lang('localize.email_error')" value="{{ old('email', $customer->email) }}" autocorrect="off" autocapitalize="off" autofocus="" required>
        </div>

        <div class="form-group">
            <label class="control-label">@lang('localize.name_as_ic') <span style="color:red;">*</span></label>
            <input type="text" name="name" class="form-control compulsary" placeholder="@lang('localize.name_error')" value="{{ old('name', $customer->cus_name) }}" autocorrect="off" autocapitalize="off" autofocus="" required>
        </div>

        <div class="form-group">
            <label class="control-label">@lang('localize.ic_number') <span style="color:red;">*</span></label>
            <input type="text" name="identity_card" class="form-control compulsary" placeholder="@lang('localize.ic_error')" value="{{ old('identity_card', $customer->identity_card) }}" autocorrect="off" autocapitalize="off" autofocus="" required>
        </div>

        <div class="form-group">
            <label class="control-label">@lang('localize.dob') <span style="color:red;">*</span></label>
            <input type="text" class="form-control compulsary" id="dob" name="dob" placeholder="@lang('localize.dob')" style="cursor: pointer;" readonly value="{{ old('dob', $customer->info? date('d-m-Y', strtotime($customer->info->cus_dob)) : null) }}">
        </div>

        <div class="form-group">
            <label class="control-label">@lang('localize.gender') <span style="color:red;">*</span></label>
            <label class="radio radio-inline">
                <input type="radio" name="gender" class="compulsary" value="1" {{ (old('cus_gender', $customer->info? $customer->info->cus_gender : 1) == 1) ? ' checked' : '' }}><ins></ins>@lang('localize.male')
            </label>
            <label class="radio radio-inline">
                <input type="radio" name="gender" class="compulsary" value="2" {{ (old('cus_gender', $customer->info? $customer->info->cus_gender : null) == 2) ? ' checked' : '' }}><ins></ins>@lang('localize.female')
            </label>
        </div>

        <div class="form-group">
            <label class="control-label">@lang('localize.address') <span style="color:red;">*</span></label>
            <input type="text" class="form-control compulsary" name="address1" placeholder="@lang('localize.address1Input')" value="{{ old('address1', $customer->cus_address1) }}">
            <input type="text" class="form-control" name="address2" placeholder="@lang('localize.address2Input')" value="{{ old('address2', $customer->cus_address2) }}">
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">@lang('localize.country') <span style="color:red;">*</span></label>
                    <select class="form-control compulsary input" id="country" name="country" onchange="get_states('#state', this.value)">
                    <option value="">@lang('localize.selectCountry')</option>
                </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">@lang('localize.state') <span style="color:red;">*</span></label>
                    <select class="form-control compulsary input" id="state" name="state">
                    <option value="">@lang('localize.selectState')</option>
                </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">@lang('localize.zipcode') <span style="color:red;">*</span></label>
                    <input type="text" class="form-control compulsary" name="zipcode" placeholder="@lang('localize.zipcode')" value="{{ old('zipcode', $customer->cus_postalcode) }}">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">@lang('localize.city') <span style="color:red;">*</span></label>
                    <input type="text" class="form-control compulsary" name="city" value="{{ old('city', $customer->cus_city_name) }}">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="checkbox">
                <input type="checkbox" name="declaration" value="1" class="form-control compulsary" required>
                <ins></ins>
                <span style="font-size:14px; cursor: pointer;">@lang('localize.declare_correct_info')</span>
            </label>
        </div>

        <button type="button" class="btn btn-primary btn-block" id="update_submit">@lang('localize.update')</button>
    </form>
</div>
@endsection

@section('styles')
<link href="{{ asset('backend/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{ asset('backend/js/plugins/datapicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script>
    $(document).ready(function() {

        get_countries('#country', "{{ old('country', $customer->cus_country? $customer->cus_country : '0') }}", '#state', "{{ old('state', $customer->cus_state? $customer->cus_state : '0') }}");

        $('#dob').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            forceParse: false,
            Default: true,
            todayHighlight: true,
        });

        $('.open-calendar').click(function(event){
            event.preventDefault();
            $('#dob').focus();
        });

        $('#update_submit').click(function() {
            var isValid = true

            $('form#update_info :input').each(function(e) {
                if ($(this).hasClass('compulsary')) {
                    if (!$(this).val()) {
                        $(this).attr('placeholder', "@lang('localize.fieldrequired')").css('border', '1px solid red').focus();
                        isValid = false;
                        return false;
                    }

                    if ($(this).attr('name') == 'declaration') {
                        if ($(this).prop('checked') == false){
                            swal ( "Oops" , "@lang('localize.confirm_correct_info')", "info" )
                            isValid = false;
                            return false;
                        }
                    }
                }

                $(this).css('border', '');
            });

            if (isValid) {
                $('#update_info').submit();
            }
        });
    });
</script>
@endsection
