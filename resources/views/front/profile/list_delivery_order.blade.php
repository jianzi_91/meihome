<button type="button" class="close hideDetailsDeliveryOrder">&times;</button>

<div class="table-responsive">
    <table class="table">
    <thead>
    <tr>
        <th nowrap class="text-left">{{ trans('localize.number') }}</th>
        <th nowrap class="text-center">{{ trans('localize.trackingno') . '/' . trans('localize.remarks') }}</th>
        <th nowrap class="text-center">{{ trans('localize.action') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($results as $result)
        <tr>
            <td class="text-left" style="width: 70%;">
                {{ $result['delivery']->version == 1? $result['delivery']->invoice->tax_number('ON') : $result['delivery']->invoice->tax_number('ORS') }}
            </td>
            <td style="width: 30%;">
                {{ $result['delivery']->tracking_number ? $result['delivery']->tracking_number : $result['delivery']->appointment_detail }}
            </td>
            <td>
                <button class="btn btn-primary btn-sm" onclick="view_deliveries_order({{ $result['delivery']->id }}, 'user', {{ $result['delivery']->version }})">
                    {{ trans('localize.view_details') }}
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
<!--TRACKING0123456-->

{{--@foreach ($results as $result)
    <table class="single-transaction">
        <tbody>
            <tr class="header">
                <td colspan="2">
                    Delivery order number: {{ $result['delivery']->do_number }}
                    <span class="pull-right">Tracking Number: {{ $result['delivery']->tracking_number }}</span>
                </td>
            </tr>
            <tr class="body">
                <td>
                    <table style="width: 100%">
                        @foreach ($result['items'] as $item)
                            @php
                                $first = $loop->first ? 'first-index' : '';
                                $inner = (!$loop->first && !$loop->last) ? 'inner-index' : '';
                                $last = $loop->last ? 'last-index' : '';
                            @endphp
                            <tr>
                                <td class="{{ $first . $inner . $last }}">
                                    <img class="product-img lazyload" data-src="{{ env('IMAGE_DIR') . '/product/' .$item->product->pro_mr_id.'/'. $item->product->image }}" src="{{ asset('assets/images/stock.png') }}">

                                    <h4>
                                        @if (isset($item->product) && isset($item->product->title))
                                            <span>
                                                {{ $item->product->title }}
                                            </span>
                                        <br/>
                                        <br/>
                                            @php
                                                if ($item->order_status == 1) {
                                                    $orderstatus = "<span class='label label-warning'>" . trans('localize.processing_order') . "</span>";
                                                } else if ($item->order_status == 2) {
                                                    if(in_array($item->order_type, [3,4,5]))
                                                        $orderstatus = "<span class='label label-primary'>" . trans('localize.pending') . "</span>";
                                                    else
                                                        $orderstatus = "<span class='label label-primary'>" . trans('localize.packaging_order') . "</span>";
                                                } else if ($item->order_status == 3) {
                                                    $orderstatus = "<span class='label label-info'>" . trans('localize.shipped_order') . "</span>";
                                                } else if ($item->order_status == 4) {
                                                    $orderstatus = "<span class='label label-success'>" . trans('localize.completed') . "</span>";
                                                } else if ($item->order_status == 5) {
                                                    $orderstatus = "<span class='label label-danger'>" . trans('localize.canceled') . "</span>";
                                                } else if ($item->order_status == 6) {
                                                    $orderstatus = "<span class='label label-primary'>" . trans('localize.refunded') . "</span>";
                                                }
                                            @endphp
                                            {!! $orderstatus !!}
                                        @endif
                                        @if(!is_null($item->order_attributes))
                                            <div class="variant">
                                                <small>
                                                    <br>
                                                    {!!$item->parseOrderAttribute !!}
                                                </small>
                                            </div>
                                        @endif
                                    </h4>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <br/>
@endforeach--}}
