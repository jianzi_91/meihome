@extends('layouts.api.plain')

@section('content')
<div class="tab-content">
    <div id="tax_inv" class="tab-pane fade in active">
        <div class="modal-header">
            <div class="row">
                <div class="col-sm-3"><img src="{{ url('/assets/images/meihome_logo.png') }}" alt="" style="width:auto; height:35px;"></div>
                <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase">@lang('localize.tax_invoice')</h3></div>
            </div>
        </div>
        <div class="modal-body">
            @if ($tax_inv)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <b>@lang('localize.bill_to') : </b>{{ $tax_inv->customer->cus_name }}<br/>
                            <b>@lang('localize.address') : </b> <br>
                            {!! ucwords(implode('<br>', array_map('trim', array_filter([$tax_inv->customer->cus_address1,$tax_inv->customer->cus_address2, $tax_inv->customer->cus_city_name])))) !!} <br>
                            {{ ucwords(trim(implode(', ', array_filter([$tax_inv->customer->cus_postalcode, $tax_inv->customer->state? $tax_inv->customer->state->name : null, $tax_inv->customer->country? $tax_inv->customer->country->co_name : null])))) }}<br>
                            <b>@lang('localize.contact_number') : </b>{{$tax_inv->customer->phone()}}<br/>
                            <br>
                            <b>@lang('localize.ship_to') : </b>{{$tax_inv->parent_order->shipping_address->ship_name}}<br/>
                        </div>
                        <div class="col-sm-6" style="border:1px solid black;margin-top:5%">
                            <b>@lang('localize.invoice_no')  :</b> ONS{{ $tax_inv->tax_number }}<br>
                            <b>@lang('localize.date')        :</b> {{ \Helper::UTCtoTZ($tax_inv->items->first()->created_at, 'd-M-Y') }}<br>
                            <b>@lang('localize.Page')        :</b> 1<br>
                            @if(!$tax_inv->deliveries->isEmpty())
                                {{-- <b>@lang('localize.do.number')   :</b>
                                ON{{ implode(', ON', $tax_inv->deliveries->pluck('do_number')->toArray()) }}
                                <br> --}}
                                ON{{ $tax_inv->tax_number }}
                            @endif
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <table border=1 width="100%">
                            <tr>
                                <th class='text-center'><b>@lang('localize.item_no')</b></th>
                                <th class='text-center'><b>@lang('localize.product_description')</b></th>
                                <th class='text-center'><b>@lang('localize.quantity')</b></th>
                                <th class='text-center'><b>@lang('localize.unit_price') ({{ $tax_inv->items->first()->currency }})</b></th>
                                <td class='text-center'><b>@lang('localize.total') ({{ $tax_inv->parent_order->items->first()->currency }})</b></td>
                            </tr>
                            <tr>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <td class='text-center'>&nbsp;</td>
                            </tr>
                            @foreach($tax_inv->items as $key=>$item)
                                <tr>
                                    <th class='text-center'>{{$loop->iteration}}</th>
                                    <th style="padding-left:10px">  {{ $item->product->pro_title_en }}</th>
                                    <th class='text-center'>{{ $item->order_qty }}</th>
                                    <th class='text-center'>{{ number_format(($item->product_original_price), 2) }}</th>
                                    <td class='text-center'>{{ number_format(($item->product_original_price * $item->order_qty), 2) }}</td>
                                </tr>
                                @if($loop->last)
                                    <tr>
                                        <th class='text-center'>{{$loop->iteration + 1}}</th>
                                        <th style="padding-left:10px">  {{trans('localize.shipping_fees')}}</th>
                                        <th class='text-center'>1</th>
                                        <th class='text-center'>{{ number_format(($item->total_product_shipping_fees_credit * $item->currency_rate), 2) }}</th>
                                        <td class='text-center'>{{ number_format(($item->total_product_shipping_fees_credit * $item->currency_rate), 2) }}&nbsp;</td>
                                    </tr>
                                @endif
                                @php
                                    $total += $item->product_original_price * $item->order_qty;
                                    $grand_total = $total + ($item->total_product_shipping_fees_credit * $item->currency_rate)
                                @endphp
                            @endforeach
                            <tr>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <td class='text-center'>&nbsp;</td>
                            </tr>
                            <tr>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <td class='text-center'>&nbsp;</td>
                            </tr>
                            <tr>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <td class='text-center'>&nbsp;</td>
                            </tr>
                            <tr>
                                <th class='text-center'>&nbsp;</th>
                                <th style="padding-left:10px">@lang('localize.grand_total') @lang('localize.inclusive_of_gst')</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <td class='text-center'>{{number_format(round($grand_total,2),2)}}</td>
                            </tr>
                            <tr>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <td class='text-center'>&nbsp;</td>
                            </tr>
                            <tr>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>&nbsp;</th>
                                <td class='text-center'>&nbsp;</td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        @if($tax_inv->parent_order->items->first()->created_at >= $date)
                            <table width="60%">
                                <tr>
                                    <th style="padding-left:5px;" class="text-center"><u>{{trans('localize.GST_summary')}}</u></td>
                                    <th style="padding-left:5px;" class="text-center"><u>@lang('localize.amount') ({{ $tax_inv->parent_order->items->first()->currency }})</u></td>
                                    <th style="padding-left:5px;" class="text-center"><u>@lang('localize.tax') ({{ $tax_inv->parent_order->items->first()->currency }})</u></td>
                                </tr>
                                <tr>
                                    <th style="padding-left:5px;" class="text-center">{{trans('localize.sr')}}</td>
                                    <th style="padding-left:5px;" class="text-center">{{ number_format($grand_total / 1.06, 2 )}}</td>
                                    <th style="padding-left:5px;" class="text-center">{{ number_format($grand_total - ($grand_total / 1.06),2)}}</td>
                                </tr>
                            </table>
                        @endif
                        <br><br>
                        <br>* This tax invoice is computer generated and no signature is required.
                    </div>
                </div>
            @else
                <div class="alert alert-danger">
                    Order you're tryin' to access is not valid because it's belong to other merchant. Please contact system admin for more details.
                </div>
            @endif

        </div>
    </div>
</div>
@endsection



