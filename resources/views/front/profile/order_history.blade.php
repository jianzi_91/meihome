@extends('layouts.web.master')

@section('header')
	@include('layouts.web.header.main')
@endsection

@section('content')
<div class="ListingTopbar">
    <h4 class="ListingCategory">@lang('localize.mybuys')</h4>
    <a href="/profile" class="back"><i class="fa fa-angle-left"></i></a>
</div>

<div class="ContentWrapper">
    @if(!empty($orders))
        @foreach($orders as $order)
            @continue($order->items->count() < 1)
            <table class="single-transaction">
                <tbody>
                    <tr class="header">
                        <td colspan="2" class="status">
                            @if($order->items->where('order_status', 1)->count() == 0)
                            @if ($order->isDeliveryOrderExist)
                            <button class="btn btn-info btn-sm"
                                    onclick="show_delivery_order(this)"
                                    data-url="{{ route('profile.order.delivery-order', [$order->id]) }}">
                                {{ $order->deliveryOrderVersion == 1 ? trans('localize.delivery_order') : trans('localize.redemption_statement') }}
                            </button>
                            @endif
                            {{-- <button class="btn btn-info btn-sm"
                                    onclick="show_tax_invoice(this)"
                                    data-url="{{ route('profile.order.tax-invoice', [$order->id]) }}">
                                {{ trans('localize.tax_invoice') }}
                            </button> --}}
                            @endif
                            <div class="pull-left">
                                @lang('localize.transID') : {{  $order->transaction_id }} &ensp; | &ensp;
                                @if ($order->paymentType == 1)
                                @lang('localize.payment') : @lang('common.credit_name')
                                @elseif ($order->paymentType == 3)
                                @lang('localize.payment') : @lang('localize.ccfpx')
                                @endif
                            </div>
                        </td>
                    </tr>
                    <tr class="body">
                        <td>
                            <table style="width: 100%">
                                <tr class="tax_do_content">
                                    <td colspan="3"></td>
                                </tr>
                                @foreach ($order->items as $item)
                                    @php
                                        $first = $loop->first ? 'first-index' : '';
                                        $inner = (!$loop->first && !$loop->last) ? 'inner-index' : '';
                                        $last = $loop->last ? 'last-index' : '';
                                    @endphp
                                    <tr>
                                        <td class="{{ $first . $inner . $last }}" rowspan="2" colspan="2">
                                            <img class="product-img lazyload" data-src="{{ env('IMAGE_DIR') . '/product/' .$item->product->pro_mr_id.'/'. $item->product->image }}" src="{{ asset('assets/images/stock.png') }}">
                                            <h4>
                                                @if (isset($item->product) && isset($item->product->title))
                                                    <span>
                                                        <div class="credit">{{ $item->order_vtokens }}<span> @lang('common.credit_name')</span></div>
                                                        {{ $item->product->title }}
                                                    </span>
                                                @endif
                                                @if(!is_null($item->order_attributes))
                                                    <div class="variant">
                                                        <small>
                                                            <br>
                                                            {!!$item->parseOrderAttribute !!}
                                                        </small>
                                                    </div>
                                                @endif
                                            </h4>
                                            @if (!empty($item->remarks))
                                                <p>@lang('localize.remarks') : {{ $item->remarks }}</p>
                                            @endif

                                            @if ($item->product_shipping_fees_type == 3)
                                                @if ($item->order_status == 4)
                                                    <p>@lang('localize.remarks') : {{ $item->order_tracking_no }}</p>
                                                @elseif ($item->order_status < 4)
                                                    <p><span><i class="fa fa-exclamation"></i> @lang('localize.member_self_pickup_disclaimer')</span></p>
                                                @endif
                                            @endif
                                        </td>
                                        <td valign="top" class="action first-index {{ $inner . $last }}">
                                            @php
                                                if ($item->order_status == 1) {
                                                    $orderstatus = "<span class='label label-warning'>" . trans('localize.processing_order') . "</span>";
                                                } else if ($item->order_status == 2) {
                                                    if(in_array($item->order_type, [3,4,5]))
                                                        $orderstatus = "<span class='label label-primary'>" . trans('localize.pending') . "</span>";
                                                    else
                                                        $orderstatus = "<span class='label label-primary'>" . trans('localize.packaging_order') . "</span>";
                                                } else if ($item->order_status == 3) {
                                                    $orderstatus = "<span class='label label-info'>" . trans('localize.shipped_order') . "</span>";
                                                } else if ($item->order_status == 4) {
                                                    $orderstatus = "<span class='label label-success'>" . trans('localize.completed') . "</span>";
                                                } else if ($item->order_status == 5) {
                                                    $orderstatus = "<span class='label label-danger'>" . trans('localize.canceled') . "</span>";
                                                } else if ($item->order_status == 6) {
                                                    $orderstatus = "<span class='label label-primary'>" . trans('localize.refunded') . "</span>";
                                                }
                                            @endphp
                                            {!! $orderstatus !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="action {{ $first . $inner . $last }}">
                                            @if ($item->order_type == 3)
                                                <button onclick="get_code_number_listing({{ $item->order_id }})" class="btn btn-primary btn-sm btn-block" style="padding: 4px 7px;margin-top: 5px;">@lang('localize.view_coupon')</button>
                                            @endif

                                            @if ($item->order_type == 4)
                                                <button onclick="get_code_number_listing({{ $item->order_id }}, 'tickets')" class="btn btn-info btn-sm btn-block" style="padding: 4px 7px;margin-top: 5px;">@lang('localize.view_ticket')</button>
                                            @endif

                                            @if ($item->order_type == 5)
                                                <button onclick="get_code_number_listing({{ $item->order_id }}, 'ecard')" class="btn btn-primary btn-sm btn-block" style="padding: 4px 7px;margin-top: 5px;">@lang('localize.e-card.serial_number')</button>
                                            @endif

                                            @if($item->order_status == 3)
                                                <button onclick="complete_order(this)" class="btn btn-primary btn-sm btn-block" url="{{ url('profile/accept_orders', [$item->order_id, 4]) }}" style="padding: 4px 7px;margin-top: 5px;">@lang('localize.shipment_is_received')</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                    </tr>
                </tbody>
                <tr class="footer">
                    <td colspan="2">
                        {{ \Helper::UTCtoTZ($order->date) }}
                    </td>
                </tr>
            </table>
        @endforeach

        <div class="PaginationContainer">
            {{ $orders->links() }}
        </div>
    @else
        <br/>
        <center>@lang('localize.table_no_record')</center>
    @endif
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content load_modal">
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('scripts')
<script src="{{ asset('assets/js/footable/footable.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/custom.js') }}" type="text/javascript"></script>
<script type="text/javascript">

    function complete_order(input)
    {
        var url = $(input).attr('url');
        input.remove();

        window.location.href = url;
    }

    function show_delivery_order(elem) {

        var
            $this = $(elem),
            url = $this.data('url');

        $('.loading').show();

        $.get(url, function(data) {

            $('.loading').hide();

            if(data == 0) {
                swal("Error!", "Invalid Operation!", "error");
            } else {
                $this.closest('.single-transaction').find('.tax_do_content td').html(data).hide().slideDown();
            }
        });
    }

    function show_tax_invoice(elem) {

        var
            $this = $(elem),
            url = $this.data('url');

        $('.loading').show();

        $.get(url, function(data) {

            $('.loading').hide();

            if(data == 0) {
                swal("Error!", "Invalid Operation!", "error");
            } else {
                $this.closest('.single-transaction').find('.tax_do_content td').html(data).hide().slideDown();
            }
        });
    }

    $('.single-transaction').on('click', '.showDetailsTaxInvoice', function () {
        var $this = $(this);
        var taxId = $this.data('taxId');
        view_tax_online_order(taxId, 'user');
    });

    // $('.single-transaction').on('click', '.showDetailsDo', function () {
    //     var $this = $(this);
    //     var doId = $this.data('doId');
    //     view_deliveries_order(doId, 'user');
    // });

    $('.single-transaction').on('click', '.hideDetailsTaxInvoice, .hideDetailsDeliveryOrder', function () {
        var $this = $(this);
        var taxId = $this.data('taxId');
        $this.closest('.single-transaction').find('.tax_do_content td').slideUp()
    });

    function get_code_number_listing(this_id, type = 'coupons') {

        var url = '/get_code_number_listing/' + this_id + '/member/' + type;

        $.get( url, function( data ) {
            if(data == 0) {
                swal("Error!", "Invalid Operation!", "error");
            } else {
                $('#myModal').modal();
                $('#myModal').on('shown.bs.modal', function(){
                    $('#myModal .load_modal').html(data);
                });
                $('#myModal').on('hidden.bs.modal', function(){
                    $('#myModal .modal-body').data('');
                });
            }
        });
    }

    function paramPage($item, $val) {
        var href = window.location.href.substring(0, window.location.href.indexOf('?'));
        var qs = window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.length);
        var newParam = $item + '=' + $val;

        if (qs.indexOf($item + '=') == -1) {
            if (qs == '') {
                qs = '?'
            }
            else {
                qs = qs + '&'
            }
            qs = newParam;
        }
        else {
            var start = qs.indexOf($item + "=");
            var end = qs.indexOf("&", start);
            if (end == -1) {
                end = qs.length;
            }
            var curParam = qs.substring(start, end);
            qs = qs.replace(curParam, newParam);
        }
        //alert(qs);
        window.location.replace(href + '?' + qs);
    }

    function paramSearchOrder($search) {
        var val = document.getElementById('searchorder').value;
        var item = document.getElementById('items').value;
        var href = window.location.href.substring(0, window.location.href.indexOf('?'));
        var qs = window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.length);
        var newParam = $search + '=' + val;

        window.location.replace(href +'?items='+ item +'&' + newParam);
    }
</script>
<script>
    jQuery(function($){
        $('.responsive-table').footable({
            empty: "@lang('localize.table_no_record')"
        });
    });
</script>
@endsection
