<button type="button" class="close hideDetailsTaxInvoice">&times;</button>

<div class="table-responsive">
    <table class="table">
    <thead>
        <tr>
            <th nowrap class="text-left">{{ trans('localize.invoice.number') }}</th>
            <th nowrap class="text-center">{{ trans('localize.item_type') }}</th>
            <th nowrap class="text-center">{{ trans('localize.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($taxInvoices as $invoice)
            <tr class="text-center">
                <td class="text-left" style="width: 90%;">{{ $invoice->tax_number() }}</td>
                <td class="text-nowrap">
                    {{ $invoice->item_type() }}
                </td>
                <td>
                    <button
                        class="btn btn-primary btn-sm showDetailsTaxInvoice" data-tax-id="{{ $invoice->id }}">
                        {{ trans('localize.view_details') }}
                    </button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
