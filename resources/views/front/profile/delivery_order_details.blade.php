@extends('layouts.api.plain')

@section('content')
    <div class="modal-header">
        <div class="row">
            <div class="col-sm-3"><img src="{{ url('/assets/images/meihome_logo.png') }}" alt="" style="width:auto; height:35px;"></div>
            <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase">@lang('localize.delivery_order')</h3></div>
        </div>
    </div>
    <div class="modal-body">
        @if ($deliveries_order->invoice && $deliveries_order->invoice->parent_order && $deliveries_order->invoice->parent_order->shipping_address)
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        @php
                            $shipping = $deliveries_order->invoice->parent_order->shipping_address;
                        @endphp
                        @if($shipping)
                            <b>@lang('localize.delivery_to') : </b>{{ $shipping->ship_name}}<br/>
                            <b>@lang('localize.delivery_address') : </b> <br>
                            {!! ucwords(implode('<br>', array_map('trim', array_filter([$shipping->ship_address1, $shipping->ship_address2, $shipping->ship_city_name])))) !!} <br>
                            {{ ucwords(trim(implode(', ', array_filter([$shipping->ship_postalcode, $shipping->state? $shipping->state->name : null, $shipping->country? $shipping->country->co_name : null])))) }}<br>
                            <b>@lang('localize.contact_number') : </b>{{ $shipping->phone() }}<br>
                            <b>@lang('localize.delivery_date') : </b>{{ \Helper::UTCtoTZ($deliveries_order->shipment_date, 'd-M-Y') }}<br/>
                            @if ($deliveries_order->courier)
                                <b>{{trans('localize.ship_company')}}: </b>{{ $deliveries_order->courier->name }}<br/>
                                <b>{{trans('localize.trackingno')}}: </b>{{ $deliveries_order->tracking_number }}<br/>
                            @else
                                <b>{{trans('localize.remarks')}}: </b>{{ $deliveries_order->appointment_detail }}<br/>
                            @endif
                        @endif
                    </div>
                    <div class="col-sm-6" style="border:1px solid black;margin-top:5%">
                        <b>@lang('localize.do.number')  :</b> {{ $deliveries_order->do_number() }}<br>
                        <b>@lang('localize.date')       :</b> {{ \Helper::UTCtoTZ($deliveries_order->created_at, 'd-M-Y') }}<br>
                        <b>@lang('localize.Page')       :</b> 1<br>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <table border=1 width="100%">
                        <tr>
                            <th class='text-center'><b>@lang('localize.item_no')</b></th>
                            <th class='text-center'><b>@lang('localize.sku_code')</b></th>
                            <th class='text-center'><b>@lang('localize.product_description')</b></th>
                            <th class='text-center'><b>@lang('localize.quantity')</b></th>
                        </tr>
                        <tr>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                        </tr>
                        @foreach($deliveries_order->items as $key=>$item)
                            <tr>
                                <th class='text-center'>{{$loop->iteration}}</th>
                                <th class='text-center'>&nbsp;</th>
                                <th class='text-center'>{{ $item->product->pro_title_en }}</th>
                                <th class='text-center'>{{ $item->order_qty }}</th>
                            </tr>
                        @endforeach
                        <tr>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                        </tr>
                        <tr>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                        </tr>
                        <tr>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>@lang('localize.total')</th>
                            <th class='text-center'>{{$deliveries_order->items->sum('order_qty')}}</th>
                        </tr>
                        <tr>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                            <th class='text-center'>&nbsp;</th>
                        </tr>
                    </table>
                    <br>
                    <br>

                    <br>* This delivery order is computer generated and no signature is required.
                </div>
            </div>
        @else
            <div class="alert alert-danger">
                Order you're tryin' to access is not valid because it's belong to other merchant. Please contact system admin for more details.
            </div>
        @endif
    </div>

@endsection



