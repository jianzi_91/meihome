<div class="row text-center" style="padding: 15px; border-bottom: 1px solid #e5e5e5;">
    <img src="{{ url('/assets/images/meihome_logo.png') }}" alt="" style="width:auto; height:125px; float: left;">
    <h3 class="text-uppercase">{{ env('COMPANY_NAME') }} <small>({{ env('COMPANY_REG_NO') }})</small></h3>
    <span style="font-size: 13px;">
    (@lang('localize.gst_id_no') : {{ env('GST') }})<br>
    {!! env('COMPANY_ADDRESS') !!} <br>
    @lang('localize.tel') : {{ env('CONTACT_NO') }}
    </span>
</div>