@if(!$address && !$batch && $customer)
    <div class="col-xs-8 taxInvoice" style="padding-left: 0;">
        <p>
            <strong>@lang('localize.merchant_id') : </strong>{{ $merchant->mer_id }}<br>
            <strong>@lang('localize.merchant_name') : </strong>{{ $merchant->full_name() }}<br>
            <strong>@lang('localize.address') : </strong> <br>
            {!! ucwords(implode('<br>', array_map('trim', array_filter([$merchant->mer_address1, $merchant->mer_address2, $merchant->mer_city])))) !!} <br>
            {{ ucwords(trim(implode(', ', array_filter([$merchant->zipcode, $merchant->state? $merchant->state->name : null, $merchant->country? $merchant->country->co_name : null])))) }} <br>

            <strong>@lang('localize.contact_number') : </strong>{{ $merchant->mer_phone }}<br>
            <strong>@lang('localize.gst_no') : </strong>{{ $merchant->bank_gst }}
        </p>
    </div>

    <div class="col-xs-8 taxInvoiceCustomer" style="display: none; padding-left: 0;">
        <p>
            <strong>@lang('localize.bill_to') : </strong> {{ $customer->cus_name }} <br>
            <strong>@lang('localize.address') : </strong> <br>
            {!! ucwords(implode('<br>', array_map('trim', array_filter([$customer->cus_address1,$customer->cus_address2, $customer->cus_city_name])))) !!} <br>
            {{ ucwords(trim(implode(', ', array_filter([$customer->cus_postalcode, $customer->state? $customer->state->name : null, $customer->country? $customer->country->co_name : null])))) }} <br>
            <strong>@lang('localize.contact_number') : </strong> {{ $customer->phone() }} <br>
            <strong>@lang('localize.email') : </strong> {{ $customer->email }}
        </p>
    </div>
@else
    <div class="col-xs-8" style="padding-left: 0;">
        @if($address == 'merchant')
        <p>
            <strong>@lang('localize.merchant_id') : </strong>{{ $merchant->mer_id }}<br>
            <strong>@lang('localize.merchant_name') : </strong>{{ $merchant->full_name() }}<br>
            <strong>@lang('localize.address') : </strong>
            {!! ucwords(implode('<br>', array_map('trim', array_filter([$merchant->mer_address1, $merchant->mer_address2, $merchant->mer_city])))) !!} <br>
            {{ ucwords(trim(implode(', ', array_filter([$merchant->zipcode, $merchant->state? $merchant->state->name : null, $merchant->country? $merchant->country->co_name : null])))) }} <br>

            <strong>@lang('localize.contact_number') : </strong>{{ $merchant->mer_phone }}<br>
            <strong>@lang('localize.gst_no') : </strong>{{ $merchant->bank_gst }}
        </p>
        @else
        <p>
            <strong>@lang('localize.bill_to') : </strong> {{ $customer->cus_name }} <br>
            <strong>@lang('localize.address') : </strong> <br>
            {!! ucwords(implode('<br>', array_map('trim', array_filter([$customer->cus_address1,$customer->cus_address2, $customer->cus_city_name])))) !!} <br>
            {{ ucwords(trim(implode(', ', array_filter([$customer->cus_postalcode, $customer->state? $customer->state->name : null, $customer->country? $customer->country->co_name : null])))) }} <br>
            <strong>@lang('localize.contact_number') : </strong> {{ $customer->phone() }}
        </p>
        @endif
    </div>
@endif