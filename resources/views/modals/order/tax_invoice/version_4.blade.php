@php
$total = $invoice->items->sum(function ($item){
    return round($item->priceAmount, 2);
});

$tax = 0;
if($gstRate > 0)
{
    $tax = round($total - ($total / (1 + ($gstRate/100))), 2);
}
$gross = round($total - $tax, 2);

$commission = ceil($gross * $invoice->merchant->mer_platform_charge/100);
$gst = ceil($gross * $gstRate/100) + ceil($commission * $gstRate/100);

$year = \Carbon\Carbon::parse($invoice->created_at)->year - 1911;
$month = \Carbon\Carbon::parse($invoice->created_at)->month;
$day = \Carbon\Carbon::parse($invoice->created_at)->day;
@endphp

<div class="text-center"><h4><strong>中華民國 {{ $year }} 年 {{ $month }} 月 {{ $day }} 日</strong></h4></div>

<div class="row">

    <div class="col-xs-12" style="padding: 0; padding-right: 15px;">
        <table>
            <tr>
                <th style="vertical-align:top;">统一编号</th>
                <th style="vertical-align:top; padding:0 3px 0;">:&ensp;</th>
                <td>{{ strtok($invoice->tw_invoice,';') }}</td>
            </tr>        
            <tr>
                <th style="vertical-align:top;" nowrap>買受人</th>
                <th style="vertical-align:top; padding:0 3px 0;">:&ensp;</th>
                <td>
                    {{ $invoice->customer->cus_name }}
                </td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>發票號碼</th>
                <th style="vertical-align:top; padding:0 3px 0;">:&ensp;</th>
                <td>
                    {{ $invoice->customer->identity_card }}
                </td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>地址</th>
                <th style="vertical-align:top; padding:0 3px 0;">:&ensp;</th>
                <td>
                    {{ $invoice->customer->cus_address1 }}
                </td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>檢查號碼</th>
                <th style="vertical-align:top; padding:0 3px 0;">:&ensp;</th>
                <td></td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>貨單號碼</th>
                <th style="vertical-align:top; padding:0 3px 0;">:&ensp;</th>
                <td>
                    {{ $invoice->tax_number }}
                </td>
            </tr>
        </table>
    </div>    
</div>

<br>

<div class="row">

    <div class="col-sm-12" style="padding:0;">
        <div class="table-responsive">
        
        <table class="table table-bordered" style="margin-bottom: 0px; border: 2px solid black">
            <tr>
                <th class="text-center" style="padding: 5px !important; width: 40%;" nowrap>品名</th>
                <th class="text-center" style="padding: 5px !important; width: 10%;" nowrap>數量</th>
                <th class="text-center" style="padding: 5px !important; width: 10%;" nowrap>單價</th>
                <th class="text-center" style="padding: 5px !important; width: 10%;" nowrap>金額</th>
                <th class="text-center" style="padding: 5px !important; width: 30%;" nowrap>備注</th>
            </tr>

            @foreach ($invoice->items as $item)
            <tr>
                <td class="text-center no-btm">{{ isset($item->product->pro_title_cnt)?$item->product->pro_title_cnt:$item->product->pro_title_en }}</td>
                <td class="text-right no-btm">{{ $item->order_qty }}</td>
                <td class="text-center no-btm">{{ number_format($item->product_price, 0) }}</td>
                <td class="text-center no-btm">{{ $item->order_qty * $item->product_price }}</td>
                @if($loop->first)
                <td class="text-left" rowspan="{{ $loop->count + 2 }}">&ensp;</td>
                @endif
            </tr>
            @endforeach

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th class="text-center">營業人蓋用統一發票專用章</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th class="text-center" rowspan="6">副本</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            <tr>
                <td class="text-center" colspan="3" style="border-top: 2px solid black">銷售額合計</td>
                <td class="text-left" style="vertical-align:middle; border-top: 2px solid black">{{ $gross }}</td>
            </tr>
            <tr>
                <td class="text-center" colspan="3">行政服务费</td>
                <td class="text-left" style="vertical-align:middle;">{{ $commission + $gst }}</td>
            </tr>
            <tr>
                <td class="text-center" colspan="3" style="border-bottom: 2px solid black">總計</td>
                <td class="text-left" style="vertical-align:middle; border-bottom: 2px solid black">{{ $gross + $commission + $gst }}</td>
            </tr>
            <tr>
                <td class="text-center" colspan="4" style="border-bottom: 2px solid black">總計新臺幣:壹仟貳佰陸拾零元整</td>
            </tr>
        </table>
        <br>
        <!-- <caption>※ 應稅、零稅率、免稅之銷售應分別開立統一發票，並應於各該欄打「V」。</caption> -->
        </div>
    </div>
</div>