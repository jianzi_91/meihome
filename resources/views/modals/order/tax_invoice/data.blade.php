@php
$currency = $invoice->items->first()->currency;
$parent = $invoice->parent_order;
$customer = $invoice->customer;
$merchant = $invoice->merchant;
$shipping = $parent? $parent->shipping_address : null;

$count = 1;
$default = $invoice->items->first();

list($whole, $decimal) = explode('.', $default->cus_service_charge_rate);
$gstRate = $decimal > 0? $default->cus_service_charge_rate : $whole;
$currencyRate = $default->currency_rate;

$from = Carbon\Carbon::create(2017, 7, 1)->startOfDay();
$to = Carbon\Carbon::create(2017, 9, 30)->endOfDay();
$inRange = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $default->created_at)->between($from, $to);

$invoice->items->map(function ($item) use ($currencyRate, $gstRate, $inRange) {
    $item->creditUnit = 0;
    if($currencyRate > 0)
    {
        $item->creditUnit = round($item->product_original_price / $currencyRate, 4);
        if(!$inRange)
        {
            $item->creditUnit = round($item->creditUnit + ($item->creditUnit * ($gstRate/100)), 4);
        }
    }
    $item->creditAmount = round($item->creditUnit * $item->order_qty, 4);
    $item->creditShippingFees = round($item->total_product_shipping_fees_credit, 4);
    $item->creditPlatformCharge = round($item->cus_platform_charge_value, 4);
    $item->creditServiceCharge = round($item->cus_service_charge_value, 4);
    $item->creditMerchantCharge = round($item->merchant_charge_vtoken, 4);
    $item->creditMerchantEarn = round(max(0, $item->order_vtokens - ($item->merchant_charge_vtoken + $item->cus_service_charge_value + $item->cus_platform_charge_value)), 4);

    $item->priceUnit = round($item->creditUnit * $currencyRate, 2);
    $item->priceAmount = round($item->creditAmount * $currencyRate, 2);
    $item->priceShippingFees = round($item->creditShippingFees * $currencyRate, 2);
    $item->pricePlatformCharge = round($item->creditPlatformCharge * $currencyRate, 2);
    $item->priceServiceCharge = round($item->creditServiceCharge * $currencyRate, 2);
    $item->priceMerchantCharge = round($item->creditMerchantCharge * $currencyRate, 2);
    $item->priceMerchantEarn = round($item->creditMerchantEarn * $currencyRate, 2);
    return $item;
});

$currentVersion = (int) $invoice->version;
if($invoice->version < 3 && $userType == 'admin')
{
   $currentVersion = (int) $version;
}

$limitDate = Carbon\Carbon::parse('31-8-2018');
$invoiceDate = Carbon\Carbon::parse($invoice->created_at);
@endphp

<div class="row">
    <div class="col-sm-12 text-center text-uppercase" style="margin-bottom:20px;margin-top:15px;">
        @if($invoiceDate > $limitDate)
            @if ($invoice->version == 4)
            <h2><strong>台灣美家優品電子商務有限公司</strong></h2>
            @else
            <h2><strong>@lang('localize.invoice_title')</strong></h2>
            @endif
        @else
            <h2><strong>@lang('localize.tax_invoice')</strong></h2>
        @endif
    </div>
</div>

<div style="font-size: 11px;">
@include('modals.order.tax_invoice.version_'. $currentVersion)
</div>

{{--
@if($operation == 'view')
@include('modals.order.tax_invoice.version_'. $version)
@include('modals.order.tax_invoice.ivEM')
@include('modals.order.tax_invoice.ivMM')

@else
    @if(!$address)
    @include('modals.order.tax_invoice.ivCustomer')
    @else
    @include('modals.order.tax_invoice.ivEM')
    @endif
@endif --}}

