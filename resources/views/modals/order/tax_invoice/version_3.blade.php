@php
$total = $invoice->items->sum(function ($item) use ($gstRate){
    return round($item->priceAmount, 2);
});

$tax = 0;
if($gstRate > 0)
{
    $tax = round($total - ($total / (1 + ($gstRate/100))), 2);
}
$amount = round($total - $tax, 2);
@endphp

<div class="row">

    <div class="col-xs-7" style="padding: 0; padding-right: 15px;">
        <table>
            <tr>
                <th style="vertical-align:top;">@lang('localize.bill_to')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>{{ $companies->{'MM'}->company_name }}</td>
            </tr>
            <tr>
                <th style="vertical-align:top;">@lang('localize.address')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>
                    {!! ucwords(implode('<br>', array_map('trim', array_filter([$companies->{'MM'}->address])))) !!}
                </td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>@lang('localize.contact_number')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>
                    {{ $companies->{'MM'}->tel }}
                </td>
            </tr>
            <tr>
                <th style="vertical-align:top;" nowrap>@lang('localize.ship_to')</th>
                <th style="vertical-align:top; padding:0 3px 0;">:</th>
                <td>
                    {{ $customer->cus_name }}
                </td>
            </tr>
        </table>
    </div>

    <div class="col-xs-5" style="border:1px solid #737373; padding: 10px;">
        <table>
            <tr>
                <th>@lang('localize.invoice_no')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td class="taxNumber" nowrap>
                    {{ $invoice->tax_number('OMP') }}
                </td>
            </tr>
            <tr>
                <th>@lang('localize.date')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>
                    {{ \Helper::UTCtoTZ($parent->date, 'd-M-Y', $isFromApi) }}
                </td>
            </tr>
            <tr>
                <th>@lang('localize.Page')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td> 1 </td>
            </tr>
            <tr>
                <th>@lang('localize.statement_no')</th>
                <th style="padding: 0 4px 0;"> : </th>
                <td>
                    {{ $invoice->tax_number('ORS') }}
                </td>
            </tr>
        </table>
    </div>
</div>

<div style="margin-top:30px;"></div>

<div class="row">

    <div class="col-sm-12" style="padding:0;">
        <div class="table-responsive">
        <table class="table table-bordered-custom">

            <tr style="background-color: #e8e8e8;">
                <th class="text-center" style="padding: 15px !important;" nowrap>@lang('localize.item_no')</th>
                <th class="text-text" style="padding: 15px !important; width:90%;" nowrap>@lang('localize.description')</th>
                <th class="text-center" style="padding: 15px !important;" nowrap>@lang('localize.quantity')</th>
                <th class="text-center" style="padding: 15px !important;" nowrap>@lang('localize.vcUnit')</th>
                <th class="text-center" style="padding: 15px !important;" nowrap>@lang('localize.amount') ({{ $currency }})</th>
            </tr>

            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>

            @foreach ($invoice->items as $item)
            <tr>
                <td class="text-center" style="vertical-align:middle;">{{ $loop->iteration }}</td>
                <td class="text-left" style="vertical-align:middle;">
                    {{ $item->product->pro_title_en }}
                    @if(!is_null($item->order_attributes))
                    <p>
                        {!! $item->parseOrderAttribute !!}
                    </p>
                    @endif

                    @if(in_array($userType, ['admin', 'user']) && $item->ecards->count() > 0)
                    <p style="margin-top:5px;">
                        <strong>@lang('localize.e-card.serial_number') : </strong><br>
                        {!! implode('<br>', $item->generatedCode->pluck('serial_number')->toArray()) !!}
                    </p>
                    @endif
                </td>
                <td class="text-center" style="vertical-align:middle;">{{ $item->order_qty }}</td>
                <td class="text-center" style="vertical-align:middle;">{{ number_format($item->creditAmount, 4) }}</td>
                <td class="text-center" style="vertical-align:middle;">{{ number_format($item->priceAmount, 2) }}</td>
            </tr>
            @endforeach

            @for ($i = 0; $i < 2; $i++)
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            @endfor

            <tr>
                <td class="text-center">&nbsp;</td>
                <td class="text-left" style="vertical-align:middle;">
                    <b>@lang('localize.grand_total')</b>
                </td>
                <td class="text-center">&nbsp;</td>
                <th class="text-center" style="vertical-align:middle; border-bottom: 7px double black; border-top: 3px solid black;">{{ number_format($invoice->items->sum(function ($item) { return $item->creditAmount; }), 4) }}</td>
                <th class="text-center" style="vertical-align:middle; border-bottom: 7px double black; border-top: 3px solid black;">{{ number_format($invoice->items->sum(function ($item) { return $item->priceAmount; }), 2) }}</td>
            </tr>

            @for ($i = 0; $i < 2; $i++)
            <tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            @endfor

        </table>
    </div>
    </div>

    <div class="col-sm-12">
        @if($parent->date >= $date)
        {{-- <table width="60%">
            <tr>
                <th class="text-center"><u>@lang('localize.GST_summary')</u></td>
                <th class="text-center"><u>@lang('localize.amount') ({{ $currency }})</u></td>
                <th class="text-center"><u>@lang('localize.tax') ({{ $currency }})</u></td>
            </tr>
            <tr>
                <th class="text-center">@lang('localize.sr', ['rate' => $gstRate])</td>
                <th class="text-center">{{ number_format(round($amount, 2), 2)}}</td>
                <th class="text-center">{{ number_format(round($tax, 2), 2) }}</td>
            </tr>
        </table> --}}
        @endif
        <br><br><br>
        * This invoice is computer generated and no signature is required.
    </div>
</div>

