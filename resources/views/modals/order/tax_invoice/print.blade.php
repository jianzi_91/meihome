<!doctype html>
<html lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8;"/>
        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.css') }}" media="all">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/custom.css') }}" media="all">

        <style type="text/css">
        .page-break {
            page-break-after: always;
            page-break-inside: avoid;
        }
        body {
            width: 100%;
            background-color: white;
            padding: 20px;
        }
        table {
            width: 100% !important;
        }
        </style>

    </head>

    <body onload="{{ !$isFromApi? 'window.print()' : '' }}">
        @if(!$batch)
        <div class="container">
            @if($invoice->version != 4)
            @include('modals.partial.company_header')
            @endif
            @include('modals.order.tax_invoice.data')
        </div>
        @else

        @php
            $invoices = $invoice;
        @endphp
        @foreach ($invoices->sortByDesc('id') as $invoice)
        <div class="container">
            @include('modals.partial.company_header')
            @include('modals.order.tax_invoice.data')
        </div>
        @if(!$loop->last)
        <div class="page-break"></div>
        @endif
        @endforeach

        @endif
    </body>
</html>
