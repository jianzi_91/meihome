<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="row">
        <div class="col-sm-3"><img src="{{ url('/assets/images/meihome_logo.png') }}" alt="" style="width:auto; height:35px;"></div>
        <div class="col-sm-7 text-center" style="vertical-align: middle;"><h3 class="text-uppercase"></h3></div>
    </div>
</div>

<div class="modal-body">
    @if(!$batch)
    @include('modals.order.tax_invoice.data')
    @else
    @php
        $invoices = $invoice;
    @endphp
    @foreach ($invoices->sortByDesc('id') as $invoice)
        @include('modals.order.tax_invoice.data')
        @if(!$loop->last)
        <hr>
        @endif
    @endforeach
    @endif
</div>

<div class="modal-footer">
    <a href="{{ $downloadLink }}" class="btn btn-primary">@lang('localize.download')</a>

    <a href="#" class="btn btn-primary" onclick="window.open('{{ $printLink }}', 'newwindow', 'width=750, height=500'); return false;">@lang('localize.print')</a>

    <button data-dismiss="modal" class="btn btn-default" type="button">@lang('localize.closebtn')</button>
</div>