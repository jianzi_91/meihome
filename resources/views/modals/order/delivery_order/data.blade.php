@php
$shipping = $delivery->invoice && $delivery->invoice->parent_order && $delivery->invoice->parent_order->shipping_address ? $delivery->invoice->parent_order->shipping_address : null;

$default = $delivery->items->first();
$currency = $default->currency;

list($whole, $decimal) = explode('.', $default->cus_service_charge_rate);
$gstRate = $decimal > 0? $default->cus_service_charge_rate : $whole;
$currencyRate = $default->currency_rate;

$from = Carbon\Carbon::create(2017, 7, 1)->startOfDay();
$to = Carbon\Carbon::create(2017, 9, 30)->endOfDay();
$inRange = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $default->created_at)->between($from, $to);

$items = $delivery->items->map(function ($item) use ($currencyRate, $gstRate, $inRange) {
    $item->creditUnit = 0;
    if($currencyRate > 0)
    {
        $item->creditUnit = round($item->product_original_price / $currencyRate, 4);
        if(!$inRange)
        {
            $item->creditUnit = round($item->creditUnit + ($item->creditUnit * ($gstRate/100)), 4);
        }
    }
    $item->creditAmount = round($item->creditUnit * $item->order_qty, 4);
    $item->creditShippingFees = round($item->total_product_shipping_fees_credit, 4);
    $item->creditPlatformCharge = round($item->cus_platform_charge_value, 4);
    $item->creditServiceCharge = round($item->cus_service_charge_value, 4);
    $item->creditMerchantCharge = round($item->merchant_charge_vtoken, 4);
    $item->creditMerchantEarn = round(max(0, $item->order_vtokens - ($item->merchant_charge_vtoken + $item->cus_service_charge_value + $item->cus_platform_charge_value)), 4);

    $item->priceUnit = round($item->creditUnit * $currencyRate, 2);
    $item->priceAmount = round($item->creditAmount * $currencyRate, 2);
    $item->priceShippingFees = round($item->creditShippingFees * $currencyRate, 2);
    $item->pricePlatformCharge = round($item->creditPlatformCharge * $currencyRate, 2);
    $item->priceServiceCharge = round($item->creditServiceCharge * $currencyRate, 2);
    $item->priceMerchantCharge = round($item->creditMerchantCharge * $currencyRate, 2);
    $item->priceMerchantEarn = round($item->creditMerchantEarn * $currencyRate, 2);
    return $item;
});
@endphp

@include('modals.order.delivery_order.version_'. $delivery->version)