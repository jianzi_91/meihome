<!doctype html>
<html lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8;"/>
        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.css') }}" media="all">
        <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/custom.css') }}" media="all">

        <style type="text/css">
        .page-break {
            page-break-after: always;
            page-break-inside: avoid;
        }
        body {
            width: 100%;
            background-color: white;
            padding: 20px;
        }
        table {
            width: 100% !important;
        }
        </style>

    </head>

    <body onload="{{ !$isFromApi? 'window.print()' : '' }}">
        @if(!$batch)
        <div class="container">
            @include('modals.partial.company_header')
            @include('modals.order.delivery_order.data')
        </div>
        @else

        @php
            $deliveries = $delivery;
        @endphp
        @foreach ($deliveries->sortByDesc('id') as $delivery)
        <div class="container">
            @include('modals.partial.company_header')
            @include('modals.order.delivery_order.data')
        </div>
        @if(!$loop->last)
        <div class="page-break"></div>
        @endif
        @endforeach

        @endif
    </body>
</html>
