<tr class="text-center">
    <td>{{ $order->order_id }}</td>
    <td>
        {{ $order->product->id }} - {{ $order->product->name }}
        @if($order->options)
        <p><br>
        @foreach ($order->options as $parent => $child)
            <b>{{ $parent }} : </b> {{ $child }} @if(!$loop->last)<br>@endif
        @endforeach
        </p>
        @endif
    </td>
    <td>{{ $order->quantity }}</td>
    <td>{{ $order->currency_code }} {{ number_format($order->total->price, 2) }}</td>
    <td>{{ number_format($order->total->credit, 4) }}</td>
    <td>
        <b>({{ $order->total->merchant_charge_rate }}%) : </b>{{ number_format($order->total->merchant_charge_credit, 4) }}
    </td>
    <td>
        <b>{{ $order->shipping_type->text }} : </b>{{ number_format($order->total->shipping_fees_credit, 4) }}
    </td>
    <td>{{ number_format($order->total->merchant_earn_credit, 4) }}</td>
    <td>{{ $order->status->text }}</td>
    @if($group == 0)
    <td>
        @if($order->order_type == 3)
        <button type="button" class="btn btn-success btn-xs btn-block btn-code-view" data-id="{{ $order->order_id }}" data-action="view_coupon">@lang('localize.view_coupon')</button>
        @elseif($order->order_type == 4)
        <button type="button" class="btn btn-success btn-xs btn-block btn-code-view" data-id="{{ $order->order_id }}" data-action="view_ticket">@lang('localize.view_ticket')</button>
        @elseif($order->order_type == 5)
        <button type="button" class="btn btn-success btn-xs btn-block btn-code-view" data-id="{{ $order->order_id }}" data-action="view_ecard">@lang('localize.e-card.view')</button>
        @endif
    </td>
    @endif
    <td width="1%">
        <div class="i-checks">
            <label>
                <input type="checkbox" class="input_checkbox" data-status="{{ $order->status->code }}" data-order-type="{{ $order->order_type }}" data-shipment-type="{{ $order->shipping_type->code }}" name="order_id[]" value="{{ $order->order_id }}" disabled>
            </label>
        </div>
    </td>
</tr>