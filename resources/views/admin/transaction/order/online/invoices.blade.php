@extends('admin.layouts.master')

@section('title', 'Tax Invoice History')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>@lang('localize.invoice.tax_history')</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.transaction')
            </li>
            <li>@lang('localize.online_orders')</li>
            <li class="active">
                <strong>@lang('localize.invoice.tax_history')</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
     <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" action="{{ url('admin/transaction/online/invoices') }}" method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['tax_number'] }}" placeholder="@lang('localize.invoice.number')" name="tax_number">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['customer_id'] }}" placeholder="@lang('localize.customer_id')" name="customer_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['merchant_id'] }}" placeholder="@lang('localize.merchant_id')" name="merchant_id">
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['transaction_id'] }}" placeholder="@lang('localize.transaction_id')" name="transaction_id">
                        </div>
                    </div> --}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.product_types')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="item_type">
                                @foreach ($item_types as $key => $value)
                                <option value="{{ !$loop->first? $key : '' }}" {{ $input['item_type'] == $key? 'selected' : '' }}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.shipment_types')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="shipment_type">
                                @foreach ($shipment_types as $key => $value)
                                <option value="{{ !$loop->first? $key : '' }}" {{ $input['shipment_type'] == $key? 'selected' : '' }}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.sort')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="new" {{ (!$input['sort'] || $input['sort'] == 'new') ? 'selected' : ''}}>@lang('localize.Newest')</option>
                                <option value="old" {{ ($input['sort'] == 'old') ? 'selected' : ''}}>@lang('localize.Oldest')</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary">@lang('localize.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            @include('admin.common.notifications')

            <div class="ibox">
                <div class="ibox-title" style="display: block;">
                    <div class="ibox-tools">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">@lang('localize.view_batch')<span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="batch" data-version="1">@lang('localize.version') 1</a></li>
                                <li><a href="#" class="batch" data-version="2">@lang('localize.version') 2</a></li>
                                <li><a href="#" class="batch" data-version="3">@lang('localize.version') 3</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center text-middle">
                                        <div class="i-checks">
                                            <label>
                                                <input type="checkbox" id="check_all">
                                            </label>
                                        </div>
                                    </th>
                                    <th class="text-center text-middle">@lang('localize.#id')</th>
                                    <th class="text-center text-middle">@lang('localize.invoice.number')</th>
                                    <th class="text-center text-middle">@lang('localize.user_info')</th>
                                    <th class="text-center text-middle">@lang('localize.product_types')</th>
                                    <th class="text-center text-middle">@lang('localize.shipment_types')</th>
                                    <th class="text-center text-middle">@lang('localize.date')</th>
                                    <th class="text-center text-middle">@lang('localize.Action')</th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach ($invoices as $invoice)
                                <tr class="text-center">
                                    <th class="text-center text-nowrap text-middle">
                                        <div class="i-checks">
                                            <label>
                                                <input type="checkbox" class="input_checkbox" name="id" value="{{ $invoice->id }}">
                                            </label>
                                        </div>
                                    </th>
                                    <td class="text-middle">{{ $invoice->id }}</td>
                                    <td class="text-middle">{{ ($invoice->version == 1 || $invoice->version == 5) ? $invoice->tax_number('ONS') : $invoice->tax_number('OMP') }}</td>
                                    <td class="text-left middle">
                                        @if($invoice->customer)
                                        <p>
                                            <b>@lang('localize.customer') : </b>
                                            {{ $invoice->customer->cus_id }} - {{ $invoice->customer->cus_name }}
                                        </p>
                                        @endif

                                        @if($invoice->merchant)
                                        <p>
                                            <b>@lang('localize.merchant_user') : </b>
                                            {{ $invoice->merchant->mer_id }} - {{ $invoice->merchant->full_name() }}
                                        </p>
                                        @endif
                                    </td>
                                    <td class="text-middle">{{ $invoice->item_type() }}</td>
                                    <td class="text-middle">{{ $invoice->shipment_type() }}</td>
                                    <td class="text-middle">{{ \Helper::UTCtoTZ($invoice->created_at) }}</td>
                                    <td class="text-middle">
                                        @if($invoice->version == 1)
                                        <div class="btn-group btn-block">
                                            <button data-toggle="dropdown" class="btn btn-white btn-block btn-sm dropdown-toggle">
                                                @lang('localize.view_details')
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" onclick="view_tax_online_order({{ $invoice->id }}, 'admin', 1)">@lang('localize.version') 1</a></li>
                                                <li><a href="#" onclick="view_tax_online_order({{ $invoice->id }}, 'admin', 2)">@lang('localize.version') 2</a></li>
                                            </ul>
                                        </div>
                                        @else
                                        <button type="button" class="btn btn-white btn-block btn-sm" onclick="view_tax_online_order({{ $invoice->id }}, 'admin', {{ $invoice->version }})"><i class="fa fa-file-text-o"></i> @lang('localize.view_details')</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $invoices])

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
<script>
$(document).ready(function() {

    $('#check_all').on('ifToggled', function(event) {
        if(this.checked == true) {
            $('.input_checkbox').iCheck('check');
        } else {
            $('.input_checkbox').iCheck('uncheck');
        }
    });

    $('.batch').on("click", function(e) {

        e.preventDefault();
        var data = $(this).data();
        var version = data.version;

        if ($('input[name=id]:checked').length > 0)
        {
            var type = 'admin';
            var invoice_ids = $('input[name=id]:checked').map(function(_, el) {
                return $(el).val();
            }).get();

            view_tax_online_order(invoice_ids, 'admin', version);
        }else{
            swal({
                title: "Please Select Multiple Invoice",
                type: "error",
                confirmButtonClass: "btn-success",
                confirmButtonText: "OK!",
                closeOnConfirm: true
                }, function(isConfirm){
            });
        }
    });

});

</script>
@endsection
