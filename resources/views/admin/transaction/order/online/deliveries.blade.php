@extends('admin.layouts.master')

@section('title', 'History')

@section('content')
 <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
        <h2>{{ (!$redemptions) ? trans('localize.do.history') : trans('localize.redemption_history') }}</h2>
        <ol class="breadcrumb">
            <li>
                @lang('localize.transaction')
            </li>
            <li>@lang('localize.online_orders')</li>
            <li class="active">
                <strong>
                    {{ (!$redemptions) ? trans('localize.do.history') : trans('localize.redemption_history') }}
                </strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight ecommerce">
     <div class="ibox float-e-margins border-bottom">
        <a class="collapse-link nolinkcolor">
            <div class="ibox-title ibox-title-filter">
                <h5>@lang('localize.Search_Filter')</h5>
                <div class="ibox-tools">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </a>
        <div class="ibox-content ibox-content-filter" style="display:none;">
            <div class="row">
                <form class="form-horizontal" action="{{ url()->current() }}" method="GET">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.Search_By')</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['do_number'] }}" placeholder="@lang('localize.number')" name="do_number">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['customer_id'] }}" placeholder="@lang('localize.customer_id')" name="customer_id">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" value="{{ $input['merchant_id'] }}" placeholder="@lang('localize.merchant_id')" name="merchant_id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.shipment_types')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="shipment_type">
                                @foreach ($shipment_types as $key => $value)
                                <option value="{{ !$loop->first? $key : '' }}" {{ $input['shipment_type'] == $key? 'selected' : '' }}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">@lang('localize.sort')</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="sort" style="font-family:'FontAwesome', sans-serif;">
                                <option value="new" {{ (!$input['sort'] || $input['sort'] == 'new') ? 'selected' : ''}}>@lang('localize.Newest')</option>
                                <option value="old" {{ ($input['sort'] == 'old') ? 'selected' : ''}}>@lang('localize.Oldest')</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-2">
                            <button type="submit" class="btn btn-block btn-outline btn-primary">@lang('localize.search')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            @include('admin.common.notifications')

            <div class="ibox">
            <div class="ibox-title" style="display: block;">
                    <div class="ibox-tools">
                        <button type="button" class="btn btn-primary btn-sm batch">@lang('localize.view_batch')</button>
                        {{-- <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">@lang('localize.view_batch')<span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="batch" data-version="1">@lang('localize.delivery_order')</a></li>
                                <li><a href="#" class="batch" data-version="2">@lang('localize.redemption_statement')</a></li>
                            </ul>
                        </div> --}}
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        <div class="i-checks">
                                            <label>
                                                <input type="checkbox" id="check_all">
                                            </label>
                                        </div>
                                    </th>
                                    <th class="text-center">@lang('localize.number')</th>
                                    <th class="text-center" nowrap>@lang('localize.user_info')</th>
                                    <th class="text-center" nowrap>@lang('localize.shipmentdetail')</th>
                                    {{-- <th class="text-center" nowrap>@lang('localize.shipment_types')</th> --}}
                                    <th class="text-center" nowrap>@lang('localize.date')</th>
                                    <th class="text-center" nowrap>@lang('localize.Action')</th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach ($deliveries as $do)

                            <tr class="text-center">
                                <th class="text-center text-nowrap">
                                    <div class="i-checks">
                                        <label>
                                            <input type="checkbox" class="input_checkbox" name="id" value="{{ $do->id }}">
                                        </label>
                                    </div>
                                </th>
                                <td class="text-nowrap">
                                    @if($do->version == 1)
                                    <p>
                                        {{ $do->invoice->tax_number('ON') }}
                                    </p>
                                    @else
                                    <p>
                                        {{ $do->invoice->tax_number('ORS') }}
                                    </p>
                                    @endif
                                </td>
                                <td class="text-left text-nowrap">
                                    @if($do->invoice && $do->invoice->customer)
                                    <p>
                                        <b>@lang('localize.customer') : </b>
                                        {{ $do->invoice->customer->cus_id }} - {{ $do->invoice->customer->cus_name }}
                                    </p>
                                    @endif

                                    @if($do->invoice && $do->invoice->merchant)
                                    <p>
                                        <b>@lang('localize.merchant_user') : </b>
                                        {{ $do->invoice->merchant->mer_id }} - {{ $do->invoice->merchant->full_name() }}
                                    </p>
                                    @endif
                                </td>
                                <td class="text-{{ !$do->type? 'center' : 'left' }}" width="35%;">
                                    <dl class="dl-horizontal" style="margin-bottom:0;">

                                        @if(!$do->type)
                                        @lang('localize.virtual_product')
                                        @else
                                        <dt>@lang('localize.shipment_types')</dt>
                                        <dd>{{ $shipment_types->{$do->type} }}</dd>
                                        @endif

                                        @if($do->type == 1)
                                        <dt>@lang('localize.trackingno')</dt>
                                        <dd>{{ $do->tracking_number }}</dd>
                                        <dt>@lang('localize.courier')</dt>
                                        <dd>{{ $do->courier? $do->courier->name : '' }}</dd>
                                        <dt>@lang('localize.trackingwebsite')</dt>
                                        <dd>{{ $do->courier? $do->courier->link : '' }}</dd>
                                        @elseif($do->type == 2)
                                        <dt>@lang('localize.remarks')</dt>
                                        <dd>{{ $do->appointment_detail }}</dd>
                                        @endif

                                        @if($do->remarks)
                                        <dt>@lang('localize.remarks')</dt>
                                        <dd>{{ $do->remarks }}</dd>
                                        @endif
                                    </dl>
                                </td>
                                <td nowrap>{{ \Helper::UTCtoTZ($do->created_at) }}</td>
                                <td width="13%">
                                    <button type="button" class="btn btn-white btn-block btn-sm" onclick="view_deliveries_order({{ $do->id }}, 'admin', {{ $do->version }})"><i class="fa fa-file-text-o"></i> @lang('localize.view_details')</button>
                                </td>
                            </tr>

                            @endforeach
                            </tbody>

                            @include('layouts.partials.table-pagination', ['listings' => $deliveries])

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
    td, th {
        vertical-align: middle !important;
    }
</style>
@endsection

@section('script')
<script src="/backend/js/custom.js"></script>
<script>
$(document).ready(function() {

    var type = 'admin';

    $('#check_all').on('ifToggled', function(event) {
        if(this.checked == true) {
            $('.input_checkbox').iCheck('check');
        } else {
            $('.input_checkbox').iCheck('uncheck');
        }
    });

    $('.batch').on("click", function(e)
    {
        e.preventDefault();
        var data = $(this).data();

        if ($('input[name=id]:checked').length > 0) {

            e.preventDefault();
            var delivery_ids = $('input[name=id]:checked').map(function(_, el) {
                return $(el).val();
            }).get();

            view_deliveries_order(delivery_ids, type, 1);
        } else {
            swal({
                title: "Please Select Multiple Invoice",
                type: "error",
                confirmButtonClass: "btn-success",
                confirmButtonText: "OK!",
                closeOnConfirm: true
                }, function(isConfirm){

            });
        }
    });

});

</script>
@endsection
