<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_country', function (Blueprint $table) {
            $table->decimal('gst_rate', 10, 2)->after('phone_country_code')->default(0);
            $table->smallInteger('display_gst')->after('gst_rate')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_country', function (Blueprint $table) {

            if (Schema::hasColumn('nm_country', 'gst_rate'))
                $table->dropColumn('gst_rate');

            if (Schema::hasColumn('nm_country', 'display_gst'))
                $table->dropColumn('display_gst');
        });
    }
}
