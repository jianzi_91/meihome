<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nm_store', function (Blueprint $table) {
            $table->smallInteger('time_restriction')->after('accept_payment')->nullable()->comment('1 => Yes, 0 => No')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nm_store', function (Blueprint $table) {
            $table->dropColumn(['time_restriction']);
        });
    }
}
