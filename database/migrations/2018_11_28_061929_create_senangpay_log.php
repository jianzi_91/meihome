<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSenangpayLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senangpay_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cus_id')->nullable();
            $table->string('cart_token', 255)->nullable();
            $table->integer('parent_order_id')->nullable();
            $table->string('msg', 100)->nullable();
            $table->string('transaction_id', 45)->nullable();
            $table->integer('ship_id')->nullable();
            $table->string('hash', 45)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('senangpay_logs');
    }
}
