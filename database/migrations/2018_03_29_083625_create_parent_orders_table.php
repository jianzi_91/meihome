<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('transaction_id');
            $table->unsignedInteger('customer_id')->default(0);

            $table->timestamp('date');

            // $table->char('currency_code', 5);
            // $table->decimal('currency_rate', 4, 2)->default(0);

            // $table->decimal('total_platform_charge_credit', 11, 4)->default(0);
            // $table->decimal('total_service_charge_credit', 11, 4)->default(0);
            // $table->decimal('total_merchant_charge_credit', 11, 4)->default(0);
            // $table->decimal('total_shipping_fees_credit', 11, 4)->default(0);
            // $table->decimal('total_merchant_earn_credit', 11, 4)->default(0);
            // $table->decimal('total_admin_earn_credit', 11, 4)->default(0);
            // $table->decimal('total_credit', 11, 4)->default(0);

            // $table->decimal('total_platform_charge_price', 11, 2)->default(0);
            // $table->decimal('total_service_charge_price', 11, 2)->default(0);
            // $table->decimal('total_merchant_charge_price', 11, 2)->default(0);
            // $table->decimal('total_shipping_fees_price', 11, 2)->default(0);
            // $table->decimal('total_merchant_earn_price', 11, 2)->default(0);
            // $table->decimal('total_admin_earn_price', 11, 2)->default(0);
            // $table->decimal('total_price', 11, 2)->default(0);

            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        Schema::table('nm_order', function (Blueprint $table) {
            $table->integer('parent_order_id')->after('order_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_orders');

        Schema::table('nm_order', function (Blueprint $table) {
            if (Schema::hasColumn('nm_order', 'parent_order_id'))
                $table->dropColumn('parent_order_id');
        });
    }
}
