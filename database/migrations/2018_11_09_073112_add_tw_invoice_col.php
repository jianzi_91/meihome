<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwInvoiceCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_invoices', function (Blueprint $table) {
            $table->string('tw_invoice', 45)->after('tax_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_invoices', function (Blueprint $table) {
            if (Schema::hasColumn('tax_invoices', 'tw_invoice'))
                $table->dropColumn('tw_invoice');
        });
    }
}
