<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOfflineCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offline_categories', function (Blueprint $table) {
            $table->smallInteger('listed')->after('featured')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offline_categories', function (Blueprint $table) {
            if (Schema::hasColumn('offline_categories', 'listed'))
                $table->dropColumn('listed');
        });
    }
}
