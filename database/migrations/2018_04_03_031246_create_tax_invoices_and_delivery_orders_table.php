<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxInvoicesAndDeliveryOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tax_number');
            $table->unsignedInteger('parent_order_id')->default(0);
            $table->unsignedInteger('customer_id')->default(0);
            $table->unsignedInteger('merchant_id')->default(0);
            $table->smallInteger('item_type')->comment('1 => Normal Product\n2 => Virtual Product (coupon, ticket, ecards)')->default(0);
            $table->smallInteger('shipment_type')->comment('0 = Not Assign\n 1 => Normal Product (Shipment type free, per product, per transaction)\n 2=> Self Pickup')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        Schema::create('delivery_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tax_invoice_id')->default(0);
            $table->string('do_number');
            $table->unsignedInteger('courier_id')->default(0);
            $table->string('tracking_number')->nullable();
            $table->text('appointment_detail')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamp('shipment_date');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        Schema::create('order_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tax_invoice_id')->default(0);
            $table->unsignedInteger('order_id')->default(0);
            $table->unsignedInteger('delivery_order_id')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_invoices');
        Schema::dropIfExists('delivery_orders');
        Schema::dropIfExists('order_mappings');
    }
}
