<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentTypeToTaxInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_invoices', function (Blueprint $table) {
            $table->smallInteger('payment_type')->after('shipment_type')->comment('1 - MeiPoints, 3 - Credit Card / FPX')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_invoices', function (Blueprint $table) {
            if (Schema::hasColumn('tax_invoices', 'payment_type'))
                $table->dropColumn('payment_type');
        });
    }
}
