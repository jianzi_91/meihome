<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            $table->decimal('credit_bf', 11, 4)->after('shipment_date')->default(0)->comment('Balance credit before forward');
            $table->decimal('credit_cf', 11, 4)->after('credit_bf')->default(0)->comment('Balance credit carry forward');
        });

        Schema::table('parent_orders', function (Blueprint $table) {
            $table->decimal('credit_bf', 11, 4)->after('customer_id')->default(0)->comment('Balance credit before forward');
            $table->decimal('credit_cf', 11, 4)->after('credit_bf')->default(0)->comment('Balance credit carry forward');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_orders', function (Blueprint $table) {
            if (Schema::hasColumn('delivery_orders', 'credit_bf'))
                $table->dropColumn('credit_bf');
                $table->dropColumn('credit_cf');
        });

        Schema::table('parent_orders', function (Blueprint $table) {
            if (Schema::hasColumn('parent_orders', 'credit_bf'))
                $table->dropColumn('credit_bf');
                $table->dropColumn('credit_cf');
        });
    }
}
