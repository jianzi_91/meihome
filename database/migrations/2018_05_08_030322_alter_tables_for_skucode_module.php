<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesForSkucodeModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('code')->nullable()->after('wallet_id');
            $table->integer('count')->default(0)->after('code');
        });

        Schema::table('nm_order', function (Blueprint $table) {
            $table->string('sku')->nullable()->after('order_attributes_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            if (Schema::hasColumn('categories', 'code'))
                $table->dropColumn('code');
        });

        Schema::table('nm_order', function (Blueprint $table) {
            if (Schema::hasColumn('nm_order', 'sku'))
                $table->dropColumn('sku');
        });
    }
}
