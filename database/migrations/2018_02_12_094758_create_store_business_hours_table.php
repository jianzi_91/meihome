<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreBusinessHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_business_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->smallInteger('day');
            $table->smallInteger('status')->comment('1 => Open, 0 => Closed')->default(1);
            $table->time('open_time')->nullable()->default(null);
            $table->time('close_time')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store_business_hours');
    }
}
