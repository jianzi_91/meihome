<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIndexing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parent_orders', function (Blueprint $table)
        {
            $table->index('customer_id', 'customer_idx');
            $table->index('transaction_id', 'transaction_idx');
        });

        Schema::table('nm_order', function (Blueprint $table)
        {
            $table->index('parent_order_id', 'parent_order_idx');
            $table->index('order_cus_id', 'order_cus_idx');
            $table->index('order_pro_id', 'order_pro_idx');
            $table->index('order_courier_id', 'order_courier_idx');
            $table->index('order_status', 'order_statusx');
            $table->index('order_type', 'order_typex');
        });

        Schema::table('nm_product', function (Blueprint $table)
        {
            $table->index('pro_mr_id', 'pro_mr_idx');
            $table->index('pro_sh_id', 'pro_sh_idx');
            $table->index('pro_title_en', 'pro_title_enx');
            $table->index('pro_type', 'pro_typex');
            $table->index('pro_status', 'pro_statusx');
        });

        Schema::table('nm_merchant', function (Blueprint $table)
        {
            $table->index('email', 'emailx');
            $table->index('mer_state', 'mer_statex');
            $table->index('mer_co_id', 'mer_co_idx');
            $table->index('bank_country', 'bank_countryx');
            $table->index('mer_staus', 'mer_stausx');
            $table->index('updater_id', 'updater_idx');

        });

        Schema::table('nm_store', function (Blueprint $table)
        {
            $table->index('stor_merchant_id', 'stor_merchant_idx');
            $table->index('stor_country', 'stor_countryx');
            $table->index('stor_state', 'stor_statex');
        });

        Schema::table('nm_customer', function (Blueprint $table)
        {
            $table->index('cus_country', 'cus_countryx');
            $table->index('cus_state', 'cus_statex');
            $table->index('limit_id', 'limit_idx');
        });

        Schema::table('nm_shipping', function (Blueprint $table)
        {
            $table->index('ship_order_id', 'ship_order_idx');
            $table->index('ship_ci_id', 'ship_ci_idx');
            $table->index('ship_state_id', 'ship_state_idx');
            $table->index('ship_cus_id', 'ship_cus_idx');
        });

        Schema::table('order_offline', function (Blueprint $table)
        {
            $table->index('wallet_id', 'wallet_idx');
            $table->index('type', 'typex');
        });

        Schema::table('nm_state', function (Blueprint $table)
        {
            $table->index('country_id', 'country_idx');
            $table->index('status', 'statusx');
        });

        Schema::table('nm_country', function (Blueprint $table)
        {
            $table->index('co_code', 'co_codex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
