<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOfflineTwInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_offline', function (Blueprint $table) {
            $table->string('tw_invoice', 45)->after('tax_inv_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_offline', function (Blueprint $table) {
            if (Schema::hasColumn('order_offline', 'tw_invoice'))
                $table->dropColumn('tw_invoice');
        });
    }
}
