<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderOfflineLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_online_log', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('parent_order_id')->default(0);
            $table->integer('order_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->smallInteger('user_type')->default(0)->comment("0 => System update, 1 => Customer, 2 => Merchant, 3 => Admin");
            
            $table->text('description')->nullable();
            $table->text('remarks')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_online_log');
    }
}
