<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class InsertCmsPageTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DELETE FROM `cms_page_type`;
        ");

        DB::statement("
            ALTER TABLE `cms_page_type` AUTO_INCREMENT = 1;
        ");

        DB::table('cms_page_type')->insert([
            'id' => 1,
            'type' => 'web',
            'name' => 'Web CMS Pages',
            'description' => null,
            'created_at' => Carbon::now('UTC'),
            'updated_at' => Carbon::now('UTC')
        ]);

        DB::table('cms_page_type')->insert([
            'id' => 2,
            'type' => 'mobile',
            'name' => 'Mobile - Ecard FAQ',
            'description' => null,
            'created_at' => Carbon::now('UTC'),
            'updated_at' => Carbon::now('UTC')
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
